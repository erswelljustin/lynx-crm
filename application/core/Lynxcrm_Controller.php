<?
	
		class Lynxcrm_Controller extends CI_Controller {

			// main location of the general template
			protected $view = 'template';
			protected $ajax = false;
			protected $download = false;
			protected $entity = 'company';
			protected $entity_id = null;
			protected $require_auth = true;
			protected $pagerEntity = 'company';
			
			public function __construct(){
				parent::__construct();
				
				if($this->require_auth){
					if(!$this->user->isLoggedIn()){
						redirect('/');
					}
				}else{
					if($this->user->isLoggedIn()){
						redirect('/find/company');
					}
				}
				
				require_once dirname(__FILE__).'/../helpers/common.php';
				require_once dirname(__FILE__).'/../libraries/view.php';
				
				if($this->view){
					$this->view = View::factory($this->view);
					$this->view->sidebar = View::factory('includes/sidebar');
					$this->view->navigation = View::factory('includes/nav');
					$this->view->content = '';
				}
			}
			
			public function __destruct(){
				if(is_array($this->ajax)){
					header('Content-Type: application/json');
					echo json_encode($this->ajax);
				}elseif($this->download){
					// ignore output as download is expected
				}elseif($this->view && $this->view instanceof View){
					View::set_global('entity', $this->entity);
					View::set_global('entity_id', $this->entity_id);
					View::set_global('pagerEntity', $this->pagerEntity);

					echo $this->view;
				}
			}
			
		}