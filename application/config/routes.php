<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Login/index';
$route['logout'] = 'Logout';

$route['find/(company|contact)'] = 'Find/index/$1';
$route['tasklist'] = 'TaskList/index';

//Settings
$route['settings'] = 'Settings/index';
$route['settings/password'] = 'Settings/password';
$route['settings/google_plugin'] = 'Settings/google_plugin';
$route['settings/gmail'] = 'Settings/gmail';
//Company
$route['company/(:any)'] = 'Company/index/$1';
//Contact
$route['contact/(:any)'] = 'Contact/index/$1';
//Bookmarks
$route['marks'] = 'Marks/index';
//Modals
$route['ajax/note/list/(:any)/(:num)'] = 'Modal/ajax_get_note_list/$1/$2';
$route['ajax/task/list/(:any)/(:num)'] = 'Modal/ajax_get_task_list/$1/$2';
$route['ajax/comm/list/(:any)/(:num)'] = 'Modal/ajax_get_comm_list/$1/$2';
$route['func/contact/address/(:num)'] = 'Contact/getCompanyAddress/$1';

$route['note/(:any)/(:num)'] = 'Modal/note/$1/$2';
$route['communication/(:any)/(:num)'] = 'Modal/communication/$1/$2';
$route['bookmark/(:any)'] = 'Modal/bookmark/$1';
$route['task/(:any)/(:num)'] = 'Modal/task/$1/$2';
$route['contacts/(:any)/(:num)'] = 'Modal/contacts/$1/$2';
$route['delete/(:any)/(:num)'] = 'Modal/delete/$1/$2';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;