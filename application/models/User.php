<?
	
	class User extends CI_Model {
		
		private static $_current_user;
		
		public function __construct(){
			parent::__construct();
			
			if(!session_id()){
				session_start();
			}
			
			if(!isset($_SESSION['auth_id']) || !$_SESSION['auth_id']){
				$_SESSION['auth_id'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20).'-'.md5($_SERVER['REMOTE_ADDR']);
			}

			if(!isset($_SESSION['user_id']) || !$_SESSION['user_id']){
				$_SESSION['user_id'] = null;
			}

			if(!self::$_current_user){
				$this->isLoggedIn();
			}
		}
		
		public function getId(){
			return isset(self::$_current_user->id) ? self::$_current_user->id : false;
		}
		
		public function getEmail(){
			return isset(self::$_current_user->user_email) ? self::$_current_user->user_email : false;
		}
		
		public function getName(){
			return isset(self::$_current_user->user_name) ? self::$_current_user->user_name : false;
		}
		
		public function getDecriptedPassword(){
			return isset(self::$_current_user->user_password) ? _encrypt(self::$_current_user->user_password,'encrypt_lynxcrm') : false;
		}
		
		public function getPassword(){
			return isset(self::$_current_user->user_password) ? self::$_current_user->user_password : false;
		}
		
		public function setPassword($new){
			$new = _encrypt($new,'encrypt_lynxcrm');
			$this->db->where('id',$this->getId())->update('crm_user',array('user_password'=>$new));
			
		}
		
		public function processLogin(){
			if(isset($_POST['action'])){
				
				$success = false;
				
				try{
					$id = $this->findByLoginDetails($_POST['email'], $_POST['password']);
					if(!$id){
						throw new Exception('Account with provided login details does not exist in the system.');
					}
					
					$this->login($id);
					$success = true;
				}catch(Exception $e){
					return $e->getMessage();
				}
				
				redirect('/');
			}
			
			return false;
		}
		
		public function isLoggedIn(){
			$user_id = $_SESSION['user_id'];
			$session_id = $_SESSION['auth_id'];
			
			if(!$user_id){
				return false;
			}

			$e = $this
				->db
				->select('user_id')
				->where('user_id = '.$this->db->escape($user_id))
				->where('id = '.$this->db->escape($session_id))
				->get('crm_user_session');
			
			if(!$e->row()){
				return false;
			}
			
			$db = $this
				->db
				->select('*')
				->where('id = '.$this->db->escape($user_id))
				->get('crm_user');
			if(!$userdata = $db->row()){
				return false;
			}
			
			self::$_current_user = $userdata;

			return true;
		}
		
		public function findByLoginDetails($email, $password){
			if(!_is_email($email)){
				return false;
			}
			
			$password = _encrypt($password, 'encrypt_lynxcrm');
			
			$db = $this
				->db
				->select('id')
				->where('user_email = '.$this->db->escape($email))
				->where('user_password = '.$this->db->escape($password))
				->get('crm_user');
			
			if($user = $db->row()){
				return $user->id;
			}
			
			return false;
		}
		
		public function login($id){
			$_SESSION['auth_id'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20).'-'.md5($_SERVER['REMOTE_ADDR']);
			$_SESSION['user_id'] = $id;

            $query = "REPLACE INTO crm_user_session (`id`, `user_id`, `ip_address`, `timestamp`) VALUES (%s,%s,%s,%s)";
            $this->db->query(sprintf(
                $query,
                $this->db->escape($_SESSION['auth_id']),
                $this->db->escape($id),
                'INET_ATON(\''.$_SERVER['REMOTE_ADDR'].'\')',
                $this->db->escape(time())
                ));
		}
		
		public function logout(){
			unset($_SESSION['auth_id']);
			unset($_SESSION['user_id']);
		}
		
		public function register($email, $password, $name){
			if(!_is_email($email)){
				return false;
			}
			
			$password = _encrypt($password, 'encrypt_lynxcrm');
			
			$insert = array(
				'user_name' => $name,
				'user_password' => $password,
				'user_email' => $email
			);
			
			$this->db->insert('crm_user', $insert);
			
			return $this->db->insert_id();
		}

		public function get($id){
			return $this->db->where('id',$id)->get('crm_user')->row();
		}
		
	}