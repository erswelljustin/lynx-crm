<?

class Note extends CI_Model {

	public function getList($entity, $entity_id){

		return $this
				->db
				->select('*')
				->where('note_entity',$entity)
				->where('note_entity_id',$entity_id)
				->where('note_deleted', '0')
				->get('crm_note')
				->result();
	}

	public function process($entity, $entity_id, $id = null){
		$response = array(
			'error' => 'Unknown Error',
			'process' => false
		);

		if($id){
			$form = $this
				->db
				->select('*')
				->where('note_entity',$entity)
				->where('note_entity_id',$entity_id)
				->where('note_id',$id)
				->where('note_deleted', '0')
				->get('crm_note')
				->row();
			if(!$form){
				$response['error'] = ' Requested note not found.';
				return $response;
			}
		}else {
			$form = (object)array(
					'note_entity' => $entity,
					'note_entity_id' => $entity_id,
					'note_user_id' => $this->user->getId(),
					'note_title' => '',
					'note_date' => time(),
					'note_text' => ''
			);
		}

		if(isset($_POST['action']) && $_POST['action'] == 'process' && !isset($_GET['remove'])){
				$form->note_title = isset($_POST['note_title']) ? htmlspecialchars($_POST['note_title']) : '';
				$form->note_date = isset($_POST['note_date']) ? $_POST['note_date']: '';
				$form->note_text = isset($_POST['note_text']) ? htmlspecialchars($_POST['note_text']) : '';
		}elseif(isset($form->note_id) && isset($_GET['confirm'])){
			$response['error'] = '';
			$response['form'] = $form;
			$response['view'] = 'partials/notes/confirm';
			return $response;
		}elseif(isset($form->note_id) && isset($_GET['remove'])) {
			$this->db->where('note_id', $form->note_id)->update('crm_note', array('note_deleted'=>'1'));
			$response['error'] = '';
			$response['process'] = true;
			$response['removed'] = true;
			return $response;
		}

		if($form->note_date && isset($_POST['action']) && $_POST['action'] == 'process') {
			if(!is_numeric($form->note_date)){
				$e = explode('/',$form->note_date);
				if(isset($e[0]) && isset($e[1])) {
					$day = $e[0];
					$month = $e[1];
					unset($e[0]);
					unset($e[1]);
					$e = implode('/',$e);
					$ee = explode(' ',$e);
					array_pop($ee);
					$form->note_date = $month . '/' . $day . '/' . implode(' ', $ee);
					$form->note_date = strtotime(trim($form->note_date));
				}
			}
		} elseif($form->note_date && !isset($_POST['action'])) {
			$form->note_date = strtotime($form->note_date);
		}

		$response['form'] = $form;

		if(isset($_POST['action']) && $_POST['action'] == 'process') {
			$response['process'] = true;

			try {
				if (!$form->note_title) {
					throw new Exception('Enter title');
				}
				if($form->note_date && date('Y',$form->note_date) < 1990){
					$form->note_date = '';
				}				
				if (!$form->note_date) {
					throw new Exception('Enter date');
				}
				if (!$form->note_text) {
					throw new Exception('Enter text');
				}

				$form->note_date = date('Y-m-d H:i:s', $form->note_date);

				if(isset($form->note_id)){
					$u = (array)$form;
					unset($u['note_id']);
					unset($u['note_entity']);
					unset($u['note_entity_id']);
					unset($u['note_user_id']);
					$this
							->db
							->where('note_id',$form->note_id)
							->update('crm_note',$u);
				}else{
					$this
						->db
						->insert('crm_note',(array)$form);
				}
				$response['error'] = '';
			} catch (Exception $e) {
				$response['error'] = $e->getMessage();
			}
		}else{
			$response['error'] = '';
		}

		return $response;
	}

}