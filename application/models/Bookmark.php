<?php

	Class Bookmark extends CI_Model
	{

		public function getBookmarks(){
			return $this->db->where('book_user_id', $this->user->getId())->get('crm_bookmark')->result();
		}

		public function process($entity, $id = null) {
			$response = array(
					'error' => 'Unknown Error',
					'process' => false
			);

			if($id){
				$form = $this
						->db
						->select('*')
						->where('book_entity',$entity)
						->where('book_id',$id)
						->get('crm_bookmark')
						->row();
				if(!$form){
					$response['error'] = ' Requested bookmark not found.';
					return $response;
				}
			}else {
				$form = (object)array(
						'book_entity' => $entity,
						'book_user_id' => $this->user->getId(),
						'book_title' => '',
						'book_date' => date('Y-m-d'),
						'book_url' => ''
				);
				if(isset($_POST['book_url'])){
					$form->book_url = $_POST['book_url'];
				}elseif(isset($_GET['book_url'])) {
					$form->book_url = $_GET['book_url'];
				}

				if($row = $this->db->query('select * from crm_bookmark where book_user_id = '.$this->user->getId().' and book_url = '.$this->db->escape($form->book_url))->row()){
					$id = $row->book_id;
					$form = $row;
				}
			}

			if(isset($_POST['action']) && $_POST['action'] == 'process' && !isset($_GET['remove'])){
				$form->book_title = isset($_POST['book_title']) ? htmlspecialchars($_POST['book_title']) : '';
			}elseif(isset($form->book_id) && isset($_GET['confirm'])){
				$response['error'] = '';
				$response['form'] = $form;
				$response['view'] = 'partials/bookmark/confirm';
				return $response;
			}elseif(isset($form->book_id) && isset($_GET['remove'])) {
				$this->db->where('book_id', $form->book_id)->delete('crm_bookmark');
				$response['error'] = '';
				$response['process'] = true;
				$response['deleted'] = true;
				return $response;
			}

			$response['form'] = $form;

			if(isset($_POST['action']) && $_POST['action'] == 'process') {
				$response['process'] = true;

				try {
					if (!$form->book_title) {
						throw new Exception('Enter title');
					}

					if(isset($_POST['book_url'])){
						$form->book_url = $_POST['book_url'];
					}elseif(isset($_GET['book_url'])){
						$form->book_url = $_GET['book_url'];
					}else{
						throw new Exception('Bookmark URL not found.');
					}

					if(isset($form->book_id)){
						$u = (array)$form;
						unset($u['book_id']);
						unset($u['book_entity']);
						unset($u['book_user_id']);
						$this
								->db
								->where('book_id',$form->book_id)
								->update('crm_bookmark',$u);
					}else{
						$this
								->db
								->insert('crm_bookmark',(array)$form);
					}
					$response['error'] = '';
				} catch (Exception $e) {
					$response['error'] = $e->getMessage();
				}
			}else{
				$response['error'] = '';
			}

			return $response;
		}

	}