<?
	
	class Settings_Model extends CI_Model {
		
		public function password(){
			$result = array();
			$result['error'] = null;
			
			if(isset($_POST['action']) && $_POST['action'] == 'process'){
				try{
					$current = isset($_POST['pw']['current']) ? $_POST['pw']['current'] : '';
					$new = isset($_POST['pw']['new']) ? $_POST['pw']['new'] : '';
					$new_repeat = isset($_POST['pw']['new_repeat']) ? $_POST['pw']['new_repeat'] : '';
					
					if(!$current){
						throw new Exception('Please enter your current password');
					}
					
					if(!$new){
						throw new Exception('Please enter your new password');
					}
					
					if(!$new_repeat){
						throw new Exception('Please repeat enter your new password');
					}
					
					if($current != $this->user->getDecriptedPassword()){
						throw new Exception('Your current password does not match your account password');
					}
					
					if($new != $new_repeat){
						throw new Exception('New password does not match repeated password');
					}
					
					$this->user->setPassword($new);
					$result['success'] = true;
				}catch(Exception $e){
					$result['error'] = $e->getMessage();
				}
			}
			
			return $result;
		}
		
	}