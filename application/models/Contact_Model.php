<?php
	Class Contact_Model extends CI_Model
	{

		public function process_delete($id){
			if(!isset($_GET['remove'])){
				return View::factory('contact/confirm');
			}

			try {
				if (!$this->db->select('cont_first_name')->where('cont_contact_id',$id)->get('crm_contact')->row()) {
					throw new Exception;
				}

				$this->db->where('cont_contact_id',$id)->update('crm_contact',array(
					'cont_deleted' => true
				));
			} catch(Exception $e){}

		}

		public function process($id) {
			$result = array();

			try {

				if ($id && !$result['contact'] = $this
						->db
						->select('*')
						->where('cont_contact_id', $id)
						->where('cont_deleted', false)
						->get('crm_contact')
						->row()
				) {
					throw new Exception;
				} elseif(!$id){
					$result['contact'] = false;
				}

				if (isset($_POST['action']) && $_POST['action'] == 'process') {
					$result = $this->update($result['contact']);
				} else {
					foreach (array('type', 'sector', 'status', 'currency', 'source', 'channel') as $field) {
						//$result['list_type'] = array();
						$query = 'SELECT distinct comp_' . $field . ' FROM crm_company GROUP BY comp_' . $field;
						$db = $this->db->query($query);
						foreach ($db->result() as $row) {
							if (preg_replace('/[^a-z0-9]/i', '', $row->{'comp_' . $field})) {
								$result['list_' . $field][] = $row->{'comp_' . $field};
							}
						}
					}
				}
			} catch(Exception $e) {
				redirect('/');
			}
			return $result;
		}

		public function process_member($id)
		{
			$result = array();

			try {

				if (!$result['contact'] = $this
						->db
						->select('*')
						->where('cont_contact_id', $id)
						->get('crm_contact')
						->row()
				) {
					throw new Exception;
				}

				if (isset($_POST['action']) && $_POST['action'] == 'process') {
					$result = $this->update($result['contact']);
				} else {
					foreach (array('type', 'sector', 'status', 'currency', 'source', 'channel') as $field) {
						//$result['list_type'] = array();
						$query = 'SELECT distinct comp_' . $field . ' FROM crm_company GROUP BY comp_' . $field;
						$db = $this->db->query($query);
						foreach ($db->result() as $row) {
							if (preg_replace('/[^a-z0-9]/i', '', $row->{'comp_' . $field})) {
								$result['list_' . $field][] = $row->{'comp_' . $field};
							}
						}
					}
				}
			} catch(Exception $e) {
				redirect('/');
			}
			return $result;
		}

		private function update($contact){
			if(!$contact){
				$contact = new stdClass;
			}

			$result = array();
			$result['ajax'] = true;

			$response = array();
			$response['error'] = 'Unknown Error';

			try{
				$contact->cont_company_id = isset($_POST['cont_company_id']) && $_POST['cont_company_id'] ? htmlspecialchars($_POST['cont_company_id']) : null;
				$contact->cont_first_name = isset($_POST['cont_first_name']) && $_POST['cont_first_name'] ? htmlspecialchars($_POST['cont_first_name']) : null;
				$contact->cont_last_name = isset($_POST['cont_last_name']) && $_POST['cont_last_name'] ? htmlspecialchars($_POST['cont_last_name']) : null;
				$contact->cont_title = isset($_POST['cont_title']) && $_POST['cont_title'] ? htmlspecialchars($_POST['cont_title']) : null;
				$contact->cont_salutation = isset($_POST['cont_salutation']) && $_POST['cont_salutation'] ? htmlspecialchars($_POST['cont_salutation']) : null;
				$contact->cont_dear = isset($_POST['cont_dear']) && $_POST['cont_dear'] ? htmlspecialchars($_POST['cont_dear']) : null;
				$contact->cont_gender = isset($_POST['cont_gender']) && $_POST['cont_gender'] ? htmlspecialchars($_POST['cont_gender']) : null;
				$contact->cont_phone = isset($_POST['cont_phone']) && $_POST['cont_phone'] ? htmlspecialchars($_POST['cont_phone']) : null;
				$contact->cont_mobile = isset($_POST['cont_mobile']) && $_POST['cont_mobile'] ? htmlspecialchars($_POST['cont_mobile']) : null;
				$contact->cont_email = isset($_POST['cont_email']) && $_POST['cont_email'] ? htmlspecialchars($_POST['cont_email']) : null;
				$contact->cont_department = isset($_POST['cont_department']) && $_POST['cont_department'] ? htmlspecialchars($_POST['cont_department']) : null;
				$contact->cont_location = isset($_POST['cont_location']) && $_POST['cont_location'] ? htmlspecialchars($_POST['cont_location']) : null;
				$contact->cont_default_address_1 = isset($_POST['cont_default_address_1']) && $_POST['cont_default_address_1'] ? htmlspecialchars($_POST['cont_default_address_1']) : null;
				$contact->cont_default_address_2 = isset($_POST['cont_default_address_2']) && $_POST['cont_default_address_2'] ? htmlspecialchars($_POST['cont_default_address_2']) : null;
				$contact->cont_default_address_3 = isset($_POST['cont_default_address_3']) && $_POST['cont_default_address_3'] ? htmlspecialchars($_POST['cont_default_address_3']) : null;
				$contact->cont_default_address_city = isset($_POST['cont_default_address_city']) && $_POST['cont_default_address_city'] ? htmlspecialchars($_POST['cont_default_address_city']) : null;
				$contact->cont_default_address_postcode = isset($_POST['cont_default_address_postcode']) && $_POST['cont_default_address_postcode'] ? htmlspecialchars($_POST['cont_default_address_postcode']) : null;
				$contact->cont_default_address_county = isset($_POST['cont_default_address_county']) && $_POST['cont_default_address_county'] ? htmlspecialchars($_POST['cont_default_address_county']) : null;
				$contact->cont_default_address_country = isset($_POST['cont_default_address_country']) && $_POST['cont_default_address_country'] ? htmlspecialchars($_POST['cont_default_address_country']) : null;
				$contact->cont_alt_address_1 = isset($_POST['cont_alt_address_1']) && $_POST['cont_alt_address_1'] ? htmlspecialchars($_POST['cont_alt_address_1']) : null;
				$contact->cont_alt_address_2 = isset($_POST['cont_alt_address_2']) && $_POST['cont_alt_address_2'] ? htmlspecialchars($_POST['cont_alt_address_2']) : null;
				$contact->cont_alt_address_3 = isset($_POST['cont_alt_address_3']) && $_POST['cont_alt_address_3'] ? htmlspecialchars($_POST['cont_alt_address_3']) : null;
				$contact->cont_alt_address_city = isset($_POST['cont_alt_address_city']) && $_POST['cont_alt_address_city'] ? htmlspecialchars($_POST['cont_alt_address_city']) : null;
				$contact->cont_alt_address_postcode = isset($_POST['cont_alt_address_postcode']) && $_POST['cont_alt_address_postcode'] ? htmlspecialchars($_POST['cont_alt_address_postcode']) : null;
				$contact->cont_alt_address_county = isset($_POST['cont_alt_address_county']) && $_POST['cont_alt_address_county'] ? htmlspecialchars($_POST['cont_alt_address_county']) : null;
				$contact->cont_alt_address_country = isset($_POST['cont_alt_address_country']) && $_POST['cont_alt_address_country'] ? htmlspecialchars($_POST['cont_alt_address_country']) : null;

				if(is_null($contact->cont_first_name)){
					throw new Exception('Company Name is a required field.');
				}
				if(is_null($contact->cont_last_name)){
					throw new Exception('Company Name is a required field.');
				}

				if(is_null($contact->cont_phone)){
					throw new Exception('Company Phone is a required field.');
				}

				if(!is_null($contact->cont_email) && !_is_email($contact->cont_email)){
					throw new Exception('There is an issue with the email field, please resolve and save again.');
				}

				if(isset($contact->cont_contact_id)){
					$i = (array)$contact;
					unset($i['cont_contact_id']);
					$this->db->where('cont_contact_id',$contact->cont_contact_id)->update('crm_contact',$i);
				}else{
					$this->db->insert('crm_contact',(array)$contact);
					$response['redirect'] = base_url().'contact/'.$this->db->insert_id();
				}
				$response['error'] = '';
			}catch(Exception $e){
				$response['error'] = $e->getMessage();
			}

			$result['response'] = $response;
			return $result;
		}

	}


