<?php
	Class Task extends CI_Model {

		public function getUserTasks() {
			$tasks = array();
			$tasks = $this
				->db
				->select('*')
				->where('task_user_id', $this->user->getId())
				->where('task_deleted', 0)
				->get('crm_task')
				->result();

			foreach ($tasks as $task) {
				$task->task_entity_name = null;
				$entity = $task->task_entity;
				if($entity == 'company') {
					$entity_name = $this->db->select('*')->where('comp_company_id', $task->task_entity_id)->get('crm_company')->row();
					$task->task_entity_name = $entity_name->comp_name;
				} else {
					$entity_name = $this->db->select('*')->where('cont_contact_id', $task->task_entity_id)->get('crm_contact')->row();
					$task->task_entity_name = $entity_name->cont_first_name.' '.$entity_name->cont_last_name;
				}
			}
			return $tasks;
		}

		public function getList($entity, $entity_id){

			return $this
				->db
				->select('*')
				->where('task_entity',$entity)
				->where('task_entity_id',$entity_id)
				->where('task_deleted', false)
				->get('crm_task')
				->result();
		}

		public function process($entity, $entity_id, $id = null){
			$response = array(
					'error' => 'Unknown Error',
					'process' => false
			);

			if($id){
				$form = $this
						->db
						->select('*')
						->where('task_entity',$entity)
						->where('task_entity_id',$entity_id)
						->where('task_id',$id)
						->where('task_deleted', false)
						->get('crm_task')
						->row();
				if(!$form){
					$response['error'] = ' Requested task not found.';
					return $response;
				}
			}else {
				$form = (object)array(
						'task_entity' => $entity,
						'task_entity_id' => $entity_id,
						'task_user_id' => $this->user->getId(),
						'task_title' => '',
						'task_type' => '',
						'task_priority' => '',
						'task_status' => '',
						'task_text' => '',
						'task_due' => time(),
						'task_deleted' => false
				);
			}

			if(isset($_POST['action']) && $_POST['action'] == 'process' && !isset($_GET['remove'])){
				$form->task_title = isset($_POST['task_title']) ? htmlspecialchars($_POST['task_title']) : '';
				$form->task_due = isset($_POST['task_due']) ? $_POST['task_due'] : '';
				$form->task_text = isset($_POST['task_text']) ? htmlspecialchars($_POST['task_text']) : '';
				$form->task_priority = isset($_POST['task_priority']) ? htmlspecialchars($_POST['task_priority']) : '';
				$form->task_status = isset($_POST['task_status']) ? htmlspecialchars($_POST['task_status']) : '';
				$form->task_type = isset($_POST['task_type']) ? htmlspecialchars($_POST['task_type']) : '';
			}elseif(isset($form->task_id) && isset($_GET['confirm'])){
				$response['error'] = '';
				$response['form'] = $form;
				$response['view'] = 'partials/tasks/confirm';
				return $response;
			}elseif(isset($form->task_id) && isset($_GET['remove'])) {
				$this->db->where('task_id', $form->task_id)->update('crm_task', array('task_deleted'=>'1'));
				$response['error'] = '';
				$response['process'] = true;
				$response['removed'] = true;
				return $response;
			}
			if($form->task_due && isset($_POST['action']) && $_POST['action'] == 'process') {
				if(!is_numeric($form->task_due)){
					$e = explode('/',$form->task_due);
					if(isset($e[0]) && isset($e[1])) {
						$day = $e[0];
						$month = $e[1];
						unset($e[0]);
						unset($e[1]);
						$e = implode('/',$e);
						$ee = explode(' ',$e);
						array_pop($ee);
						$form->task_due = $month . '/' . $day . '/' . implode(' ', $ee);
						$form->task_due = strtotime(trim($form->task_due));
					}
				}
			} elseif($form->task_due && !isset($_POST['action'])) {
				$form->task_due = strtotime($form->task_due);
			}

			$response['form'] = $form;

			if(isset($_POST['action']) && $_POST['action'] == 'process') {
				$response['process'] = true;

				try {
					if (!$form->task_title) {
						throw new Exception('Enter title');
					}
					if($form->task_due && date('Y',$form->task_due) < 1990){
						$form->task_due = '';
					}
					if (!$form->task_due) {
						throw new Exception('Enter due date');
					}
					if (!$form->task_priority) {
						throw new Exception('Enter priority');
					}
					if (!$form->task_type) {
						throw new Exception('Enter type of task');
					}
					if (!$form->task_text) {
						throw new Exception('Enter task text');
					}


					$form->task_due = date('Y-m-d H:i:s', $form->task_due);

					if(isset($form->task_id)){
						$u = (array)$form;
						unset($u['task_id']);
						unset($u['task_entity']);
						unset($u['task_entity_id']);
						unset($u['task_user_id']);
						$this
								->db
								->where('task_id',$form->task_id)
								->update('crm_task',$u);
					}else{
						$this
								->db
								->insert('crm_task',(array)$form);
					}
					$response['error'] = '';
				} catch (Exception $e) {
					$response['error'] = $e->getMessage();
				}
			}else{
				$response['error'] = '';
			}

			return $response;
		}

		public function getConfiguration($type){
			$config = array(
				'priority' => array(
					'Normal',
					'Important',
					'Urgent'
				),
				'status' => array(
					'Inactive',
					'Active',
					'Over Due',
					'Completed'
				),
				'type' => array(
					'task',
					'meeting',
					'call',
					'email',
					'todo',
					'other'
				)
			);

			$c = isset($config[$type]) ? $config[$type]: array();
			foreach($c as $a=>$b) {
				$key = md5($b);
				$name = ucwords($b);

				unset($c[$a]);
				$c[$key] = $name;
			}

			return $c;
		}


	}