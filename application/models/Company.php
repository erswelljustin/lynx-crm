<?php
Class Company extends CI_Model {

    public function getCompanyNameById($id)
    {
        $this->db->select('comp_name')
                 ->from('company')
                 ->where('comp_company_id', $id);

        $comp_name = $this->db->get()->row();

        if($comp_name != '') {
            return $comp_name;
        } else {
            return false;
        }
    }

}