<?
	
		class Finder extends CI_Model {

			private $entity = null;
			private $_encryption_name = 'LynxCrmEncryptionKey';
			
			private $recordsPerPage = 15;

			// Company Entity Options
			private $companyTable = 'crm_company';
			private $companyOrderColumnName = 'comp_name';
			private $companyFields = array(
				'comp_name' => 'name',
				'comp_alt_name' => 'alt_name',
				'comp_type' => 'type',
				'comp_default_address_postcode' => 'default_postcode',
				'comp_status' => 'status',
				'comp_channel' => 'channel',
				'comp_source' => 'source',
				'comp_sector' => 'sector'
			);
			private $companyListableFields = array(
				'comp_type',
				'comp_status',
				'comp_channel',
				'comp_source',
				'comp_sector'
			);
			private $companyCustomFields = array();

			// Company Entity Options
			private $contactTable = 'crm_contact';
			private $contactOrderColumnName = 'cont_last_name';
			private $contactFields = array(
					'cont_first_name' => 'first_name',
					'cont_last_name' => 'last_name',
					'cont_department' => 'department',
					'cont_location' => 'location'
			);
			private $contactListableFields = array(
				'company'
			);
			private $contactCustomFields = array(
				'company'
			);


			public function generate($entity){
				$form = $this->formValues($entity);
				$form = json_encode($form);
				$key = _encrypt($form, $this->_encryption_name);
				return urlencode(str_replace('/','LynxCRM',base64_encode($key)));
			}
			
			public function formValues($entity, $key = false){
				$this->entity = $entity;

				$form = (object)array();

				if(!isset($this->{$entity.'Fields'})){
					redirect('/');
					return;
				}

				$fields = $this->{$entity.'Fields'};

				foreach($fields as $field){
					$form->$field = isset($_POST['find'][$field]) ? htmlspecialchars($_POST['find'][$field]) : '';
				}

				$fields = $this->{$entity.'CustomFields'};

				foreach($fields as $field){
					$form->$field = isset($_POST['find'][$field]) ? htmlspecialchars($_POST['find'][$field]) : '';
				}

				if(isset($_POST['action']) && $_POST['action'] == 'process'){
					return $form;
				}

				if($key){
					if(!$key = base64_decode(str_replace('LynxCRM','/',$key))){
						return $form;
					}
					
					if(!$data = _encrypt($key, $this->_encryption_name)){
						return $form;
					}
					if(!$data = json_decode($data)){
						return $form;
					}
					return $data;
				}
				
				return $form;
			}
			
			public function process($entity, $form, $offset = 0){

				// Predefine variables
				$lists = array();
				$rows = array();
				$pager = '';

				// Get results
				$test = implode('',(array)$form);

				$this->db->select($this->_encryption_name)->from($this->{$entity.'Table'});
				$this->db->where(($entity=='company'?'comp_':'cont_').'deleted',false);

				foreach(array_merge($this->{$entity.'Fields'},$this->{$entity.'CustomFields'}) as $column => $field){
					if(!empty($form->$field)){
						if(in_array($field, $this->{$entity.'CustomFields'})) {
							$this->{'customField' . ucfirst($field)}($form->$field);
						}else {
							$this->db->where($column . ' like ' . $this->db->escape('%' . $form->$field . '%'));
						}
					}
				}

				$query = $this->db->get_compiled_select();

				$this->processPager($query, $pager, $offset);
				$db = $this->db->query(str_replace('`'.$this->_encryption_name.'`', '*', $query).' ORDER BY '.$this->{$entity.'OrderColumnName'}.' LIMIT '.$offset.','.$this->recordsPerPage);

				foreach($db->result() as $row){
					$rows[] = $row;
				}

				foreach($this->{$entity.'ListableFields'} as $field){
					if(in_array($field, $this->{$entity.'CustomFields'})){
						$this->{'customField'.ucfirst($field).'List'}($lists);
					}else {
						$list = array();
						$query = 'SELECT distinct ' . $field . ' FROM ' . $this->{$entity . 'Table'} . ' GROUP BY ' . $field;
						$db = $this->db->query($query);
						foreach ($db->result() as $row) {
							if (preg_replace('/[^a-z0-9]/i', '', $row->$field)) {
								$list[] = $row->$field;
							}
						}
						$lists[$field] = $list;
					}
				}

				foreach($this->{$entity.'CustomFields'} as $field){
					if(in_array($field, $this->{$entity.'ListableFields'})){
						foreach($rows as $a => $b){
							if(!empty($test)){
								$rows[$a]->$field = isset($lists[$field][$form->$field]) ? $lists[$field][$form->$field] : '';
							}else{
								$rows[$a]->$field = $this->{'customField'.ucfirst($field).'DefaultValue'}($lists[$field], $rows[$a]);
							}
						}
					}
				}

				return array(
					'processed' => !empty($test),
					'results' => $rows,
					'pager' => $pager,
					'lists' => (object)$lists
				);
			}
			
			private function processPager($query, &$pager, $offset){
				$count = str_replace('`'.$this->_encryption_name.'`', 'count(0) as cnt', $query);
				$count = $this->db->query($count);
				$count = $count->row()->cnt;

				$pager = View::factory('includes/pager',array(
					'total' => $count,
					'current' => $offset,
					'counter' => $this->recordsPerPage
				));
			}

			private function customFieldCompanyDefaultValue($list, $obj){
				return isset($list[$obj->cont_company_id]) ? $list[$obj->cont_company_id] : '';
			}

			private function customFieldCompany($id){
				if($this->entity == 'contact'){
					$this->db->where('cont_company_id = ' . $this->db->escape($id));
				}
			}

			private function customFieldCompanyList(&$lists){
				$list = array();
				$query = 'SELECT comp_company_id, comp_name, comp_default_address_1, comp_default_address_city, comp_default_address_postcode FROM crm_company WHERE comp_deleted = 0 ORDER BY comp_name ASC';
				$db = $this->db->query($query);
				foreach ($db->result() as $row) {
					$name = array();
					if(!empty($row->comp_name)) {
						$name[] = $row->comp_name;
					}

					if(!empty($row->comp_default_address_1)) {
						$name[] = $row->comp_default_address_1;
					}

					if(!empty($row->comp_default_address_city)) {
						$name[] = $row->comp_default_address_city;
					}

					if(!empty($row->comp_default_address_postcode)) {
						if(count($name)){
							$row->comp_default_address_postcode = '('.$row->comp_default_address_postcode.')';
						}
						$name[] = $row->comp_default_address_postcode;
					}

					if($row->comp_name){
						array_shift($name);
						$name = implode(', ', $name);
						$name = $row->comp_name.($name ? ' - '.$name : '');
					} else {
						$name = implode(', ', $name);
					}

					if (preg_replace('/[^a-z0-9]/i', '', $name)) {
						$list[$row->comp_company_id] = $name;
					}
				}
				$lists['company'] = $list;
			}

		}