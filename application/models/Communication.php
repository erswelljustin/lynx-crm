<?

	class Communication extends CI_Model {

		public function getList($entity, $entity_id){

			return $this
				->db
				->select('*')
				->where('comm_entity',$entity)
				->where('comm_entity_id',$entity_id)
				->where('comm_deleted', false)
				->get('crm_communication')
				->result();
		}

		public function process($entity, $entity_id, $id = null){
			$response = array(
				'error' => 'Unknown Error',
				'process' => false
			);

			if($id){
				$form = $this
					->db
					->select('*')
					->where('comm_entity',$entity)
					->where('comm_entity_id',$entity_id)
					->where('comm_id',$id)
					->where('comm_deleted', false)
					->get('crm_communication')
					->row();
				if(!$form){
					$response['error'] = ' Requested task not found.';
					return $response;
				}
			}else {
				$form = (object)array(
					'comm_entity' => $entity,
					'comm_entity_id' => $entity_id,
					'comm_user_id' => $this->user->getId(),
					'comm_title' => '',
					'comm_type' => '',
					'comm_text' => '',
					'comm_date' => time(),
					'comm_deleted' => false
				);
			}

			if(isset($_POST['action']) && $_POST['action'] == 'process' && !isset($_GET['remove'])){
				$form->comm_title = isset($_POST['comm_title']) ? htmlspecialchars($_POST['comm_title']) : '';
				$form->comm_date = isset($_POST['comm_date']) ? $_POST['comm_date'] : '';
				$form->comm_text = isset($_POST['comm_text']) ? htmlspecialchars($_POST['comm_text']) : '';
				$form->comm_type = isset($_POST['comm_type']) ? htmlspecialchars($_POST['comm_type']) : '';
			}elseif(isset($form->comm_id) && isset($_GET['confirm'])){
				$response['error'] = '';
				$response['form'] = $form;
				$response['view'] = 'partials/comms/confirm';
				return $response;
			}elseif(isset($form->comm_id) && isset($_GET['remove'])) {
				$this->db->where('comm_id', $form->comm_id)->update('crm_communication', array('comm_deleted'=>'1'));
				$response['error'] = '';
				$response['process'] = true;
				$response['removed'] = true;
				return $response;
			}
			if($form->comm_date && isset($_POST['action']) && $_POST['action'] == 'process') {
				if(!is_numeric($form->comm_date)){
					$e = explode('/',$form->comm_date);
					if(isset($e[0]) && isset($e[1])) {
						$day = $e[0];
						$month = $e[1];
						unset($e[0]);
						unset($e[1]);
						$e = implode('/',$e);
						$ee = explode(' ',$e);
						array_pop($ee);
						$form->comm_date = $month . '/' . $day . '/' . implode(' ', $ee);
						$form->comm_date = strtotime(trim($form->comm_date));
					}
				}
			} elseif($form->comm_date && !isset($_POST['action'])) {
				$form->comm_date = strtotime($form->comm_date);
			}

			$response['form'] = $form;

			if(isset($_POST['action']) && $_POST['action'] == 'process') {
				$response['process'] = true;

				try {
					if (!$form->comm_title) {
						throw new Exception('Enter title');
					}
					if($form->comm_date && date('Y',$form->comm_date) < 1990){
						$form->comm_date = '';
					}
					if (!$form->comm_date) {
						throw new Exception('Enter a date');
					}
					if (!$form->comm_type) {
						throw new Exception('Enter type of task');
					}
					if (!$form->comm_text) {
						throw new Exception('Enter task text');
					}


					$form->comm_date = date('Y-m-d H:i:s', $form->comm_date);

					if(isset($form->comm_id)){
						$u = (array)$form;
						unset($u['comm_id']);
						unset($u['comm_entity']);
						unset($u['comm_entity_id']);
						unset($u['comm_user_id']);
						$this
							->db
							->where('comm_id',$form->comm_id)
							->update('crm_communication',$u);
					}else{
						$this
							->db
							->insert('crm_communication',(array)$form);
					}
					$response['error'] = '';
				} catch (Exception $e) {
					$response['error'] = $e->getMessage();
				}
			}else{
				$response['error'] = '';
			}

			return $response;
		}

		public function getConfiguration($type){
			$config = array(
				'type' => array(
					'phone',
					'email',
					'meeting',
					'skype',
					'other'
				)
			);

			$c = isset($config[$type]) ? $config[$type]: array();
			foreach($c as $a=>$b) {
				$key = md5($b);
				$name = ucwords($b);

				unset($c[$a]);
				$c[$key] = $name;
			}

			return $c;
		}


	}