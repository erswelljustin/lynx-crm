<?php

Class Company_Model extends CI_Model {

	public function process_delete($id){
		if(!isset($_GET['remove'])){
			return View::factory('company/confirm');
		}

		try {
			if (!$this->db->select('comp_name')->where('comp_company_id',$id)->get('crm_company')->row()) {
				throw new Exception;
			}

			$this->db->where('comp_company_id',$id)->update('crm_company',array(
				'comp_deleted' => true
			));
		} catch(Exception $e){}

	}

	public function getContactsForCompany($id, $exclude = null) {
		$this->db->select('*')
				->where('cont_company_id', $id)
				->where('cont_deleted', '0');

		if(!is_array($exclude) && is_numeric($exclude)){
			$exclude = array($exclude);
		}

		if(count($exclude)){
			$this->db->where('cont_contact_id not in ('.implode(',',$exclude).')');
		}

		$contacts = $this->db->get('crm_contact')->result();

		return $contacts;
	}

	public function getDefaultAddress($id) {
		$this->db->select('comp_default_address_1, comp_default_address_2, comp_default_address_3, comp_default_address_city, comp_default_address_postcode, comp_default_address_county, comp_default_address_country')
				->where('comp_company_id', $id);

		return $this->db->get('crm_company')->row();
	}

	public function getCompanyNameById($id)
	{
		$this->db->select('comp_name')
				->where('comp_company_id', $id);

		$comp_name = $this->db->get('crm_company')->row();

		if($comp_name != '') {
			return $comp_name;
		} else {
			return false;
		}
	}

	public function process($id) {
		$result = array();

		try{
			if($id && !$result['company'] = $this
					->db
					->select('*')
					->where('comp_company_id', $id)
					->where('comp_deleted', false)
					->get('crm_company')
					->row()){
				throw new Exception;
			}elseif(!$id){
				$result['company'] = false;
			}

			if(isset($_POST['action']) && $_POST['action'] == 'process'){
				$result = $this->update($result['company']);
			}else{
				foreach(array('type','sector','status','currency','source','channel','acct_manager') as $field) {
					//$result['list_type'] = array();
					$query = 'SELECT distinct comp_'.$field.' FROM crm_company GROUP BY comp_'.$field;
					$db = $this->db->query($query);
					foreach ($db->result() as $row) {
						if (preg_replace('/[^a-z0-9]/i', '', $row->{'comp_'.$field})) {
							$result['list_'.$field][] = $row->{'comp_'.$field};
						}
					}
				}
			}
		} catch(Exception $e) {
			redirect('/');
		}
		return $result;
	}

	private function update($company){
		if(!$company){
			$company = new stdClass;
		}

		$result = array();
		$result['ajax'] = true;

		$response = array();
		$response['error'] = 'Unknown Error';

		try{
			$company->comp_name = isset($_POST['comp_name'])  ? htmlspecialchars($_POST['comp_name']) : null;
			$company->comp_alt_name = isset($_POST['comp_alt_name']) && $_POST['comp_alt_name'] ? htmlspecialchars($_POST['comp_alt_name']) : null;
			$company->comp_type = isset($_POST['comp_type']) && $_POST['comp_type'] ? htmlspecialchars($_POST['comp_type']) : null;
			$company->comp_status = isset($_POST['comp_status']) && $_POST['comp_status'] ? htmlspecialchars($_POST['comp_status']) : null;
			$company->comp_source = isset($_POST['comp_source']) && $_POST['comp_source'] ? htmlspecialchars($_POST['comp_source']) : null;
			$company->comp_channel = isset($_POST['comp_channel']) && $_POST['comp_channel'] ? htmlspecialchars($_POST['comp_channel']) : null;
			$company->comp_sector = isset($_POST['comp_sector']) && $_POST['comp_sector'] ? htmlspecialchars($_POST['comp_sector']) : null;
			$company->comp_phone = isset($_POST['comp_phone']) && $_POST['comp_phone'] ? htmlspecialchars($_POST['comp_phone']) : null;
			$company->comp_fax = isset($_POST['comp_fax']) && $_POST['comp_fax'] ? htmlspecialchars($_POST['comp_fax']) : null;
			$company->comp_email = isset($_POST['comp_email']) && $_POST['comp_email'] ? htmlspecialchars($_POST['comp_email']) : null;
			$company->comp_website = isset($_POST['comp_website']) && $_POST['comp_website'] ? htmlspecialchars($_POST['comp_website']) : null;
			$company->comp_currency = isset($_POST['comp_currency']) && $_POST['comp_currency'] ? htmlspecialchars($_POST['comp_currency']) : null;
			$company->comp_terms = isset($_POST['comp_terms']) && $_POST['comp_terms'] ? htmlspecialchars($_POST['comp_terms']) : null;
			$company->comp_sage_customer_id = isset($_POST['comp_sage_customer_id']) && $_POST['comp_sage_customer_id'] ? htmlspecialchars($_POST['comp_sage_customer_id']) : null;
			$company->comp_sage_supplier_id = isset($_POST['comp_sage_supplier_id']) && $_POST['comp_sage_supplier_id'] ? htmlspecialchars($_POST['comp_sage_supplier_id']) : null;
			$company->comp_discount_5k = isset($_POST['comp_discount_5k']) && $_POST['comp_discount_5k'] ? htmlspecialchars($_POST['comp_discount_5k']) : null;
			$company->comp_discount_yb = isset($_POST['comp_discount_yb']) && $_POST['comp_discount_yb'] ? htmlspecialchars($_POST['comp_discount_yb']) : null;
			$company->comp_default_address_1 = isset($_POST['comp_default_address_1']) && $_POST['comp_default_address_1'] ? htmlspecialchars($_POST['comp_default_address_1']) : null;
			$company->comp_default_address_2 = isset($_POST['comp_default_address_2']) && $_POST['comp_default_address_2'] ? htmlspecialchars($_POST['comp_default_address_2']) : null;
			$company->comp_default_address_3 = isset($_POST['comp_default_address_3']) && $_POST['comp_default_address_3'] ? htmlspecialchars($_POST['comp_default_address_3']) : null;
			$company->comp_default_address_city = isset($_POST['comp_default_address_city']) && $_POST['comp_default_address_city'] ? htmlspecialchars($_POST['comp_default_address_city']) : null;
			$company->comp_default_address_postcode = isset($_POST['comp_default_address_postcode']) && $_POST['comp_default_address_postcode'] ? htmlspecialchars($_POST['comp_default_address_postcode']) : null;
			$company->comp_default_address_county = isset($_POST['comp_default_address_county']) && $_POST['comp_default_address_county'] ? htmlspecialchars($_POST['comp_default_address_county']) : null;
			$company->comp_default_address_country = isset($_POST['comp_default_address_country']) && $_POST['comp_default_address_country'] ? htmlspecialchars($_POST['comp_default_address_country']) : null;
			$company->comp_invoice_address_1 = isset($_POST['comp_invoice_address_1']) && $_POST['comp_invoice_address_1'] ? htmlspecialchars($_POST['comp_invoice_address_1']) : null;
			$company->comp_invoice_address_2 = isset($_POST['comp_invoice_address_2']) && $_POST['comp_invoice_address_2'] ? htmlspecialchars($_POST['comp_invoice_address_2']) : null;
			$company->comp_invoice_address_3 = isset($_POST['comp_invoice_address_3']) && $_POST['comp_invoice_address_3'] ? htmlspecialchars($_POST['comp_invoice_address_3']) : null;
			$company->comp_invoice_address_city = isset($_POST['comp_invoice_address_city']) && $_POST['comp_invoice_address_city'] ? htmlspecialchars($_POST['comp_invoice_address_city']) : null;
			$company->comp_invoice_address_postcode = isset($_POST['comp_invoice_address_postcode']) && $_POST['comp_invoice_address_postcode'] ? htmlspecialchars($_POST['comp_invoice_address_postcode']) : null;
			$company->comp_invoice_address_county = isset($_POST['comp_invoice_address_county']) && $_POST['comp_invoice_address_county'] ? htmlspecialchars($_POST['comp_invoice_address_county']) : null;
			$company->comp_invoice_address_country = isset($_POST['comp_invoice_address_country']) && $_POST['comp_invoice_address_country'] ? htmlspecialchars($_POST['comp_invoice_address_country']) : null;
			$company->comp_alt_address_1 = isset($_POST['comp_alt_address_1']) && $_POST['comp_alt_address_1'] ? htmlspecialchars($_POST['comp_alt_address_1']) : null;
			$company->comp_alt_address_2 = isset($_POST['comp_alt_address_2']) && $_POST['comp_alt_address_2'] ? htmlspecialchars($_POST['comp_alt_address_2']) : null;
			$company->comp_alt_address_3 = isset($_POST['comp_alt_address_3']) && $_POST['comp_alt_address_3'] ? htmlspecialchars($_POST['comp_alt_address_3']) : null;
			$company->comp_alt_address_city = isset($_POST['comp_alt_address_city']) && $_POST['comp_alt_address_city'] ? htmlspecialchars($_POST['comp_alt_address_city']) : null;
			$company->comp_alt_address_postcode = isset($_POST['comp_alt_address_postcode']) && $_POST['comp_alt_address_postcode'] ? htmlspecialchars($_POST['comp_alt_address_postcode']) : null;
			$company->comp_alt_address_county = isset($_POST['comp_alt_address_county']) && $_POST['comp_alt_address_county'] ? htmlspecialchars($_POST['comp_alt_address_county']) : null;
			$company->comp_alt_address_country = isset($_POST['comp_alt_address_country']) && $_POST['comp_alt_address_country'] ? htmlspecialchars($_POST['comp_alt_address_country']) : null;

			if(is_null($company->comp_name)){
				throw new Exception('Company Name is a required field.');
			}

			if(is_null($company->comp_phone)){
				throw new Exception('Company Phone is a required field.');
			}

			if(!is_null($company->comp_email) && !_is_email($company->comp_email)){
				throw new Exception('There is an issue with the email field, please resolve and save again.');
			}

			if(isset($company->comp_company_id)){
				$i = (array)$company;
				unset($i['comp_company_id']);
				$this->db->where('comp_company_id',$company->comp_company_id)->update('crm_company',$i);
			}else{
				$this->db->insert('crm_company',(array)$company);
				$response['redirect'] = base_url().'company/'.$this->db->insert_id();
			}
	$response['error'] = '';
		}catch(Exception $e){
			$response['error'] = $e->getMessage();
		}

		$result['response'] = $response;
		return $result;
	}

	public function getList() {
		$list = array();
		$query = 'SELECT comp_company_id, comp_name, comp_default_address_1, comp_default_address_city, comp_default_address_postcode FROM crm_company ORDER BY comp_name ASC';
		$db = $this->db->query($query);
		foreach ($db->result() as $row) {
			$name = array();
			if(!empty($row->comp_name)) {
				$name[] = $row->comp_name;
			}

			if(!empty($row->comp_default_address_1)) {
				$name[] = $row->comp_default_address_1;
			}

			if(!empty($row->comp_default_address_city)) {
				$name[] = $row->comp_default_address_city;
			}

			if(!empty($row->comp_default_address_postcode)) {
				if(count($name)){
					$row->comp_default_address_postcode = '('.$row->comp_default_address_postcode.')';
				}
				$name[] = $row->comp_default_address_postcode;
			}

			if($row->comp_name){
				array_shift($name);
				$name = implode(', ', $name);
				$name = $row->comp_name.($name ? ' - '.$name : '');
			} else {
				$name = implode(', ', $name);
			}

			if (preg_replace('/[^a-z0-9]/i', '', $name)) {
				$list[$row->comp_company_id] = $name;
			}
		}

		return $list;
	}

}