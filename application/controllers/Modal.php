<?php

	class Modal extends Lynxcrm_Controller
	{

		public function ajax_get_note_list($entity, $entity_id){
			$this->ajax = array(
				'view' => (string)View::factory('partials/notes/note_list', array(
					'entity' => $entity,
					'entity_id' => $entity_id,
					'rows' => $this->note->getList($entity, $entity_id)
				))
			);
		}

		public function ajax_get_task_list($entity, $entity_id){
			$this->ajax = array(
				'view' => (string)View::factory('partials/tasks/task_list_entity', array(
					'entity' => $entity,
					'entity_id' => $entity_id,
					'rows' => $this->task->getList($entity, $entity_id)
				))
			);
		}

		public function ajax_get_comm_list($entity, $entity_id){
			$this->ajax = array(
				'view' => (string)View::factory('partials/comms/comms_list', array(
					'entity' => $entity,
					'entity_id' => $entity_id,
					'rows' => $this->communication->getList($entity, $entity_id)
				))
			);
		}

		public function note($entity, $entity_id){
			$ajax = $this->note->process($entity,$entity_id,(isset($_GET['id'])?$_GET['id']:null));
			if($ajax['process']){
				$ajax['entity'] = $entity;
				$ajax['entity_id'] = $entity_id;
				$this->ajax = $ajax;
				return;
			}

			if($ajax['error']){
				die($ajax['error']);
			}

			$title = isset($ajax['form']->note_id) ? 'Edit note' : 'Add note';
			$template = 'partials/notes/note';
			$options = array(
					'entity'=>$entity,
					'entity_id' => $entity_id,
					'form' => $ajax['form']
			);

			$close = $submit = true;

			if(isset($ajax['view'])){
				$template = $ajax['view'];
				$options['ajax'] = $ajax;
				$close = 'No';
				$submit = 'Yes';
				$title = 'Confirm';
			}

			$this->entity = $entity;
			$this->entity_id = $entity_id;
			$this->view = View::factory('includes/modal_template', array(
					'title' => $title,
					'body' => $this->load->view($template, $options, TRUE),
						'buttons' => array(
						'close' => $close,
						'submit' => $submit
						)
					)
				);
			}

		public function bookmark($entity){
			$ajax = $this->bookmark->process($entity, (isset($_GET['id'])?$_GET['id']:null));
			if($ajax['process']){
				$ajax['entity'] = $entity;
				$this->ajax = $ajax;
				return;
			}

			if($ajax['error']){
				die($ajax['error']);
			}

			$title = isset($ajax['form']->book_id) ? 'Edit bookmark' : 'Add bookmark';
			$template = 'partials/bookmark/main';
			$options = array(
					'entity'=>$entity,
					'form' => $ajax['form']
			);

			$close = $submit = true;

			if(isset($ajax['view'])){
				$template = $ajax['view'];
				$options['ajax'] = $ajax;
				$close = 'No';
				$submit = 'Yes';
				$title = 'Confirm';
			} elseif(isset($_GET['id'])) {
				$delete = 'bookmark/'.$entity.'?id='.$_GET['id'].'&confirm';
			}

			$this->entity = $entity;

			$buttons = array(
				'close' => $close,
				'submit' => $submit
			);

			if(isset($delete)) {
				$buttons['delete'] = $delete;
			}

			$this->view = View::factory('includes/modal_template', array(
					'title' => $title,
					'body' => $this->load->view($template, $options, TRUE),
						'buttons' => $buttons
					)
				);
			}

		public function communication($entity, $entity_id){
			$ajax = $this->communication->process($entity,$entity_id,(isset($_GET['id'])?$_GET['id']:null));
			if($ajax['process']){
				$ajax['entity'] = $entity;
				$ajax['entity_id'] = $entity_id;
				$ajax['x'] = $_POST;
				$this->ajax = $ajax;
				return;
			}

			if($ajax['error']){
				die($ajax['error']);
			}

			$title = isset($ajax['form']->comm_id) ? 'Edit communication' : 'Add communication';
			$template = 'partials/comms/comm';
			$options = array(
				'entity'=>$entity,
				'entity_id' => $entity_id,
				'form' => $ajax['form']
			);

			$close = $submit = true;

			if(isset($ajax['view'])){
				$template = $ajax['view'];
				$options['ajax'] = $ajax;
				$close = 'No';
				$submit = 'Yes';
				$title = 'Confirm';
			}

			$this->entity = $entity;
			$this->entity_id = $entity_id;
			$this->view = View::factory('includes/modal_template', array(
					'title' => $title,
					'body' => $this->load->view($template, $options, TRUE),
					'buttons' => array(
						'close' => $close,
						'submit' => $submit
					)
				)
			);
		}

		public function task($entity, $entity_id){
			$ajax = $this->task->process($entity,$entity_id,(isset($_GET['id'])?$_GET['id']:null));
			if($ajax['process']){
				$ajax['entity'] = $entity;
				$ajax['entity_id'] = $entity_id;
				$ajax['x'] = $_POST;
				$this->ajax = $ajax;
				return;
			}

			if($ajax['error']){
				die($ajax['error']);
			}

			$title = isset($ajax['form']->task_id) ? 'Edit task' : 'Add task';
			$template = 'partials/tasks/task';
			$options = array(
				'entity'=>$entity,
				'entity_id' => $entity_id,
				'form' => $ajax['form']
			);

			$close = $submit = true;

			if(isset($ajax['view'])){
				$template = $ajax['view'];
				$options['ajax'] = $ajax;
				$close = 'No';
				$submit = 'Yes';
				$title = 'Confirm';
			}

			$this->entity = $entity;
			$this->entity_id = $entity_id;
			$this->view = View::factory('includes/modal_template', array(
							'title' => $title,
							'body' => $this->load->view($template, $options, TRUE),
							'buttons' => array(
									'close' => $close,
									'submit' => $submit
							)
					)
			);
		}

		public function delete($entity, $entity_id) {
			$this->entity = $entity;
			$this->entity_id = $entity_id;
			$obj = $this->{$entity.'_model'};
			if(!$this->view = $obj->process_delete($entity_id)){
				$this->ajax = array(
					'deleted' => true
				);
				return;
			}

			$template = $entity.'/confirm';
			$close = 'No';
			$submit = 'Yes';
			$title = 'Confirm';


			$this->view = View::factory('includes/modal_template', array(
					'title' => $title,
					'body' => $this->load->view($template,array(
						'entity_id' => $entity_id
					), TRUE),
					'buttons' => array(
						'close' => $close,
						'submit' => $submit
					)
				)
			);

		}


	}