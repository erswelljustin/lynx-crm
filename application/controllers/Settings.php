<?php

	class Settings extends Lynxcrm_Controller {
	
		public function index(){
	 		redirect('/settings/password');
		}
	
		public function password(){
			$this->entity = 'settings';
	 		$this->entity_id = 'password';
	 		
	 		$this->view->content = View::factory('settings/password', $this->settings_model->password());
		}

		public function google_plugin(){
			$gp = google_plugin();
			$gp->authorise($this->user->getId());

			if(isset($_GET['GOOGLE_PLUGIN_ALLOW_REDIRECT_AWAY'])){
				redirect('/settings/gmail');
			}

			return $gp;
		}

		public function gmail(){
			$gp = $this->google_plugin();

			$this->view->content = View::factory('settings/gmail', array(
				'google' => $gp
			));
		}

	}