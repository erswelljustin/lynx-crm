<?

	class Company extends Lynxcrm_Controller {

    public function index($id = null){
			$this->entity = 'company';
	    $this->entity_id = intval($id);
			$result = $this->company_model->process($this->entity_id);

			if(isset($result['ajax'])){
				$this->ajax = $result['response'];
				return;
			}

			if($this->entity_id) {
				$result['contactList'] = $this->company_model->getContactsForCompany($this->entity_id);

				$result['noteList'] = $this->note->getList($this->entity,$this->entity_id);
				$result['taskList'] = $this->task->getList($this->entity,$this->entity_id);
				$result['commList'] = $this->communication->getList($this->entity,$this->entity_id);
			}else{
				$this->view->navigation->only_save = true;
			}

			$this->view->content = View::factory('company/main', $result);
    }

	}
