<?php

class Contact extends Lynxcrm_Controller {

	public function index($id = null){
		$this->entity = 'contact';
 		$this->entity_id = intval($id);
		$result = $this->contact_model->process($this->entity_id);

		if(isset($result['ajax'])){
			$this->ajax = $result['response'];
			return;
		}

		$result['companyList'] = $this->company_model->getList();

		if($this->entity_id) {
			$result['noteList'] = $this->note->getList($this->entity,$this->entity_id);
			$result['taskList'] = $this->task->getList($this->entity,$this->entity_id);
			$result['commList'] = $this->communication->getList($this->entity,$this->entity_id);
			$result['contactList'] = $this->company_model->getContactsForCompany($result['contact']->cont_company_id,$this->entity_id);
		} else {
			$this->view->navigation->only_save = true;
		}
		$this->view->content = View::factory('contact/main', $result);
	}

	public function getCompanyAddress($id) {
		$this->entity = null;
		$this->entity_id = null;
		$this->view = null;
		if(!is_null($id)) {
			//debug($this->company_model->getDefaultAddress($id));
			echo json_encode($this->company_model->getDefaultAddress($id));
		} else {
			$err = array('status'=>'No id provided');
			echo json_encode($err);
		}
	}
        
}