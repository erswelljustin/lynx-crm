<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Root extends CI_Controller {

    public function index()
    {

        $data = array(
            'entity' => 'company',
            'entity_id' => '1'
        );

        $this->load->view('includes/header', $data);
        $this->load->view('main');
        $this->load->view('includes/footer');
    }

    public function note($entity, $entity_id)
    {
        $data = array(
            'title' => 'New Note',
            'body' => $this->load->view('partials/notes/note', array('entity'=>$entity, 'entity_id' => $entity_id), TRUE),
            'buttons' => array(
                'close' => true,
                'submit' => true
            )
        );

        $this->load->view('includes/modal_template', $data);
    }

    public function communication($entity, $entity_id)
    {
        $data = array(
            'title' => 'New Communication',
            'body' => $this->load->view('partials/comms/comm', array('entity'=>$entity, 'entity_id' => $entity_id), TRUE),
            'buttons' => array(
                'close' => true,
                'submit' => true
            )
        );

        $this->load->view('includes/modal_template', $data);
    }

    public function contacts($entity, $entity_id)
    {
        $comp_name = $this->company->getCompanyNameById($entity_id);
        $data = array(
            'title' => 'New Contact for '.$comp_name,
            'body' => $this->load->view('partials/contacts/contact', array('entity'=>$entity, 'entity_id' => $entity_id), TRUE),
            'buttons' => array(
                'close' => true,
                'submit' => true
            )
        );

        $this->load->view('includes/modal_template', $data);
    }
}
