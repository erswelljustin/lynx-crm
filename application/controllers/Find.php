<?

	class Find extends Lynxcrm_Controller {

		public function index($entity){
			//debug($_SERVER);

			if(isset($_SERVER['PATH_INFO'])){
				$uri = explode('/',$_SERVER['PATH_INFO']);
			} else {
				$uri = explode('/',$_SERVER['REDIRECT_QUERY_STRING']); 
			}	
			//$uri = explode('/',$_SERVER['PATH_INFO']);
			$this->entity = 'find';
			$this->pagerEntity = $uri[2];

			$key = isset($_GET['s']) ? $_GET['s'] : null;
			$offset = isset($_GET['offset']) ? intval($_GET['offset']) : 0;

			// Disable navigation menu
			//$this->view->navigation = false;
			
			// Set the content template
			$template = $entity == 'company' ? 'main' : 'contact';

			// If a new search submit requested generate search key
			if(isset($_POST['action']) && $_POST['action'] == 'process'){
				$key = $this->finder->generate($entity);
				redirect('/find/'.$entity.'?s='.$key);
				return;
			}
			
			// Get form values
			$form = $this->finder->formValues($entity, $key);
			
			// Get results
			$options = $this->finder->process($entity, $form, $offset);
			$options['form'] = $form;
			$this->view->content = View::factory('find/'.$template, $options);

			// Fin.
		}

	}
