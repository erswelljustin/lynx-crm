<?

	class TaskList extends Lynxcrm_Controller
	{

		public function index(){
			$result = array();
			$this->entity = 'tasklist';
			$result['results'] = $this->task->getUserTasks();

			//debug($result, $_SESSION);

			if(isset($result['ajax'])){
				$this->ajax = $result['response'];
				return;
			}

			$this->view->content = View::factory('partials/tasks/task_list_user', $result);
		}

	}