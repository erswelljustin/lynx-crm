<?php

Class Marks extends Lynxcrm_Controller
{

	public function index(){
		$result = array();
		$this->entity = 'bookmark';
		$result['results'] = $this->bookmark->getBookmarks();

		//debug($result, $_SESSION);

		if(isset($result['ajax'])){
			$this->ajax = $result['response'];
			return;
		}

		$this->view->content = View::factory('partials/bookmark/list', $result);

	}

}