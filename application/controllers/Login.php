<?
	
	class Login extends Lynxcrm_Controller {
		
		protected $require_auth = false;
		protected $view = 'login';
		
		public function index(){
			$this->view->error = $this->user->processLogin();

			if(isset($_GET['register'])){
				$this->user->register($_GET['email'], $_GET['password'], $_GET['name']);
				redirect('/');
			}
		}


	}