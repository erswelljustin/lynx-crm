<?

	class View {
		protected static $_global_data=array();
		protected static $_folder = null;
		protected $_file;
		protected $_data = array();
		
		public static function configureFolder($folder){
			self::$_folder = $folder;
		}
		
		public static function factory($file=null, array $data=null){
			return new View($file, $data);
		}
		
		protected static function capture($kohana_view_filename, array $kohana_view_data){
			extract($kohana_view_data, EXTR_SKIP);

			if(View::$_global_data){
				extract(View::$_global_data, EXTR_SKIP | EXTR_REFS);
			}

			ob_start();

			try{
				include $kohana_view_filename;
			}catch(Exception $e){
				ob_end_clean();
				throw $e;
			}

			return ob_get_clean();
		}
		
		public static function set_global($key, $value=null){
			if(is_array($key)){
				foreach ($key as $key2 => $value){
					View::$_global_data[$key2]=$value;
				}
			}else{
				View::$_global_data[$key]=$value;
			}
		}
		
		public static function bind_global($key, & $value){
			View::$_global_data[$key] =& $value;
		}
		public function __construct($file=null, array $data=null){
			if($file !== null){
				$this->set_filename($file);
			}

			if($data !== null){
				$this->_data=$data + $this->_data;
			}
		}
		
		public function & __get($key){
			if(array_key_exists($key, $this->_data)){
				return $this->_data[$key];
			}elseif(array_key_exists($key, View::$_global_data)){
				return View::$_global_data[$key];
			}else{
				die('View variable is not set: '.$key);
			}
		}
		
		public function __set($key, $value){
			$this->set($key, $value);
		}
		
		public function __isset($key){
			return (isset($this->_data[$key]) OR isset(View::$_global_data[$key]));
		}
		
		public function __unset($key){
			unset($this->_data[$key], View::$_global_data[$key]);
		}
		
		public function __toString(){
			try{
				return $this->render();
			}catch(Exception $e){
				die('Error: '.$e->getMessage());
			}
		}
		public function set_filename($file){
			$path = APPPATH.'views/'.$file.'.php';
			if(is_file($path)){

			}
			if(!$path){
				die('The requested view '.$file.' could not be found');
			}

			$this->_file = $path;
			return $this;
		}
		
		public function set($key, $value=null){
			if(is_array($key)){
				foreach ($key as $name => $value){
					$this->_data[$name]=$value;
				}
			}else{
				$this->_data[$key]=$value;
			}

			return $this;
		}
		
		public function bind($key, & $value){
			$this->_data[$key] =& $value;

			return $this;
		}
		
		public function render($file=null){
			if($file !== null){
				$this->set_filename($file);
			}

			if(empty($this->_file)){
				die('You must set the file to use within your view before rendering');
			}

			return View::capture($this->_file, $this->_data);
		}
		
	}
