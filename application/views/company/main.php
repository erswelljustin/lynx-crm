<form method="post" class="form-horizontal" id="<?=$entity?>Form">
	  <input type="hidden" name="action" value="process" />
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="form-group form-group-sm">
                    <label for="comp_name" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="comp_name" name="comp_name" placeholder="Company Name" value="<?=($company ? $company->comp_name : '')?>">
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_name_alt" class="col-sm-3 control-label">Alt Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="comp_alt_name" name="comp_alt_name" placeholder="Company Name" value="<?=($company ? $company->comp_alt_name : '')?>">
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_type" class="col-sm-3 control-label">Type</label>
                    <div class="col-sm-9">
                        <select name="comp_type" id="comp_type" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($list_type as $value){ ?>
														<option<?=($company && $company->comp_type == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_status" class="col-sm-3 control-label">Status</label>
                    <div class="col-sm-9">
                        <select name="comp_status" id="comp_status" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($list_status as $value){ ?>
														<option<?=($company && $company->comp_status == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_source" class="col-sm-3 control-label">Source</label>
                    <div class="col-sm-9">
                        <select name="comp_source" id="comp_source" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($list_source as $value){ ?>
														<option<?=($company && $company->comp_source == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_channel" class="col-sm-3 control-label">Channel</label>
                    <div class="col-sm-9">
                        <select name="comp_channel" id="comp_channel" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($list_channel as $value){ ?>
														<option<?=($company && $company->comp_channel == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_sector" class="col-sm-3 control-label">Sector</label>
                    <div class="col-sm-9">
                        <select name="comp_sector" id="comp_sector" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($list_sector as $value){ ?>
														<option<?=($company && $company->comp_sector == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_acct_manager" class="col-sm-3 control-label">Acct Manager</label>
                    <div class="col-sm-9">
                        <select name="comp_acct_manager" id="comp_acct_manager" class="form-control selectpicker">
                            <option value="">Choose one…</option>
                            <? foreach($list_acct_manager as $value){ ?>
                            <option<?=($company && $company->comp_acct_manager == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>                
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="form-group form-group-sm">
                    <label for="comp_default_address_1" class="col-sm-3 control-label">Default Address</label>
                    <div class="col-sm-9 address-group">
                        <input type="text" class="form-control input-sm" id="comp_default_address_1" name="comp_default_address_1" placeholder="Default address 1" value="<?=($company ? $company->comp_default_address_1 : '')?>">
                        <input type="text" class="form-control input-sm" id="comp_default_address_2" name="comp_default_address_2" placeholder="Default address 2" value="<?=($company ? $company->comp_default_address_2 : '')?>">
                        <input type="text" class="form-control input-sm" id="comp_default_address_3" name="comp_default_address_3" placeholder="Default address 3" value="<?=($company ? $company->comp_default_address_3 : '')?>">
                        <input type="text" class="form-control input-sm" name="comp_default_address_city" id="comp_default_address_city" placeholder="City" value="<?=($company ? $company->comp_default_address_city : '')?>"/>
                        <input type="text" class="form-control input-sm" name="comp_default_address_postcode" id="comp_default_address_postcode" placeholder="Post Code" value="<?=($company ? $company->comp_default_address_postcode : '')?>"/>
                        <input type="text" name="comp_default_address_county" id="comp_default_address_county" class="form-control typeahead input-sm" placeholder="County" value="<?=($company ? $company->comp_default_address_county : '')?>"/>
                            <input type="text" name="comp_default_address_country" id="comp_default_address_country" class="form-control input-sm" placeholder="Country" value="<?=($company ? $company->comp_default_address_country : '')?>"/>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_phone" class="col-sm-3 control-label">Phone</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="comp_phone" name="comp_phone" placeholder="Primary Phone" value="<?=($company ? $company->comp_phone : '')?>">
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_fax" class="col-sm-3 control-label">Fax</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="comp_fax" name="comp_fax" placeholder="Fax" value="<?=($company ? $company->comp_fax : '')?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="form-group form-group-sm">
                    <label for="comp_invoice_address_1" class="col-sm-3 control-label">Invoice Address</label>
                    <div class="col-sm-9 address-group">
                        <input type="text" class="form-control input-sm" id="comp_invoice_address_1" name="comp_invoice_address_1" placeholder="invoice address 1" value="<?=($company ? $company->comp_invoice_address_1 : '')?>">
                        <input type="text" class="form-control input-sm" id="comp_invoice_address_2" name="comp_invoice_address_2" placeholder="invoice address 2" value="<?=($company ? $company->comp_invoice_address_2 : '')?>">
                        <input type="text" class="form-control input-sm" id="comp_invoice_address_3" name="comp_invoice_address_3" placeholder="invoice address 3" value="<?=($company ? $company->comp_invoice_address_3 : '')?>">
                        <input type="text" class="form-control input-sm" name="comp_invoice_address_city" id="comp_invoice_address_city" placeholder="City" value="<?=($company ? $company->comp_invoice_address_city : '')?>"/>
                            <input type="text" class="form-control input-sm" name="comp_invoice_address_postcode" id="comp_invoice_address_postcode" placeholder="Post Code" value="<?=($company ? $company->comp_invoice_address_postcode : '')?>"/>
                        <input type="text" name="comp_invoice_address_county" id="comp_invoice_address_county" class="form-control typeahead input-sm" placeholder="County" value="<?=($company ? $company->comp_invoice_address_county : '')?>"/>
                        <input type="text" name="comp_invoice_address_country" id="comp_invoice_address_country" class="form-control input-sm" placeholder="Country" value="<?=($company ? $company->comp_invoice_address_country : '')?>"/>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control input-sm" id="comp_email" name="comp_email" placeholder="Email Address" value="<?=($company ? $company->comp_email : '')?>">
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="comp_website" class="col-sm-3 control-label">Website</label>
                    <div class="col-sm-9">
                        <input type="url" class="form-control input-sm" id="comp_website" name="comp_website" placeholder="Website" value="<?=($company ? $company->comp_website : '')?>">
                    </div>
                </div>
            </div>
        </div>
				<div class="clearfix"></div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Credit Information
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group form-group-sm">
                                    <label for="comp_currency" class="col-sm-3 control-label">Currency</label>
                                    <div class="col-sm-9">
                                        <select name="comp_currency" id="comp_currency" class="form-control selectpicker">
																					<option value="">Choose one…</option>
																					<? foreach($list_currency as $value){ ?>
																						<option<?=($company && $company->comp_currency == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
																					<? } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group form-group-sm">
                                    <label for="comp_terms" class="col-sm-3 control-label">Terms</label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" name="comp_terms" id="comp_terms" class="bs-select"<?=($company && strtolower($company->comp_terms) == 'yes'?' checked="checked"':'')?> value="yes" data-on-text="Yes" data-off-text="No"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group form-group-sm">
                                    <label for="comp_sage_id" class="col-sm-3 control-label">Sage #</label>
                                    <div class="col-sm-9">
																				<? if($company && $company->comp_type == 'Supplier') {?>
                                        	<input type="text" name="comp_sage_supplier_id" id="comp_sage_supplier_id" class="form-control input-sm" value="<?=($company ? $company->comp_sage_supplier_id : '')?>"/>
																				<? } else { ?>
																					<input type="text" name="comp_sage_customer_id" id="comp_sage_customer_id" class="form-control input-sm" value="<?=($company ? $company->comp_sage_customer_id : '')?>"/>
																				<? } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group form-group-sm">
                                    <label for="comp_discount_5k" class="col-sm-3 control-label">Disc 5K</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="comp_discount_5k" id="comp_discount_5k" class="form-control input-sm" value="<?=($company ? $company->comp_discount_5k : '')?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group form-group-sm">
                                    <label for="comp_discount_yb" class="col-sm-3 control-label">Disc YB</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="comp_discount_yb" id="comp_discount_yb" class="form-control input-sm" value="<?=($company ? $company->comp_discount_yb : '')?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
				<div class="clearfix"></div>
    </div>
	<? if ($entity_id){ ?>
    <div class="container-fluid">
					<div class="row">
							<div class="col-sm-12">
							<div role="tabpanel">

									<!-- Nav tabs -->
									<ul class="nav nav-tabs" role="tablist">
											<li role="presentation"><a data-tab-set-active href="#Contacts" aria-controls="Contacts" role="tab" data-toggle="tab">Contacts</a></li>
											<li role="presentation"><a href="#Notes" aria-controls="Notes" role="tab" data-toggle="tab">Notes</a></li>
											<li role="presentation"><a href="#Communications" aria-controls="Communications" role="tab" data-toggle="tab">Comms</a></li>
											<li role="presentation"><a href="#Tasks" aria-controls="Tasks" role="tab" data-toggle="tab">Tasks</a></li>
									</ul>

									<!-- Tab panes -->
									<div class="tab-content">
											<div role="tabpanel" class="tab-pane fade active" id="Contacts">
													<div class="row">
															<div class="col-lg-12">
																	<?=View::factory('partials/contacts/contact_list', array('rows'=>$contactList))?>
															</div>
													</div>
													<div class="clearfix"></div>
											</div>
											<div role="tabpanel" class="tab-pane fade in" id="Notes">
													<div class="row">
															<div class="col-lg-12">
																	<?=View::factory('partials/notes/note_list', array('rows'=>$noteList))?>
															</div>
													</div>
													<div class="clearfix"></div>
											</div>
											<div role="tabpanel" class="tab-pane fade" id="Communications">
													<div class="row">
															<div class="col-lg-12">
																	<?=View::factory('partials/comms/comms_list', array('rows'=>$commList))?>
															</div>
													</div>
													<div class="clearfix"></div>
											</div>
											<div role="tabpanel" class="tab-pane fade" id="Tasks">
												<div class="row">
													<div class="col-lg-12">
														<?=View::factory('partials/tasks/task_list_entity', array('rows'=>$taskList))?>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
									</div>

							</div>
					</div>
				</div>
				<div class="clearfix"></div>
		</div>
	<? } ?>
</form>
