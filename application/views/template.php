<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
		<base url="<?=base_url()?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Lynx technik UK Ltd. | CRM</title>
    <!-- Bootstrap Styles & 3rd Party Libs -->
    <!-- <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" charset="utf-8"/> -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.paper.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-select.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-switch.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-datetimepicker.min.css" charset="utf-8"/>
    <!-- End -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/animate.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/lynx.crm.css" charset="utf-8"/>
    <!-- JQuery $(function(){}); anywhere -->
    <script>(function(w,d,u){w.readyQ=[];w.bindReadyQ=[];function p(x,y){if(x=="ready"){w.bindReadyQ.push(y);}else{w.readyQ.push(x);}};var a={ready:p,bind:p};w.$=w.jQuery=function(f){if(f===d||f===u){return a}else{p(f)}}})(window,document)</script>
</head>
<body>
    <div id="wrapper">
        <?=$sidebar?>
        <div id="page-content-wrapper">
            <div class="page-content">
                <!-- Top Navbar -->
                <?=$navigation?>
                <?=$content?>
            </div>
        </div>
    </div>

    <script type="text/javascript">
			var entity = '<?=$entity?>';
    </script>
    
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/jquery-2.1.3.min.js"></script>
    <script>(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)</script>
    <!-- Bootstrap Scripts & 3rd Party Libs -->
		<script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/moment.js"></script>
		<script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap-select.min.js"></script>
		<script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap3-typeahead.min.js"></script>
		<script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?=base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" charset="UTF-8" src="<?=base_url()?>assets/js/addressLookup.js"></script>
    <!-- End -->
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/caineCustomSelectPicker.min.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/lynx.crm.js"></script>

    <!-- Modal -->
    <div class="modal fade" id="crm_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

            </div>
        </div>
    </div>
    <script type="text/template" id="modal-template">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Loading…</h4>
        </div>
        <div class="modal-body"></div>
    </script>

</body>
</html>