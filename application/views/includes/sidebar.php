    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand"><a href="<?=base_url()?>"><img src="<?=base_url();?>assets/img/lynx-logo.png" alt="Lynx Technik UK Ltd." class="img-responsive"/></a></li>
            <li>
                <a data-toggle="collapse" href="#findMenu" aria-expanded="false" aria-controls="findMenu"><i class="fa fa-search"></i> Find</a>
                <ul class="sidebar-nav-sub collapse <?=(get_instance()->router->fetch_class() == 'Find'?'in':'')?>" id="findMenu">
                    <li><a href="<?=base_url()?>find/company"><i class="fa fa-building-o"></i> Company</a></li>
                    <li><a href="<?=base_url()?>find/contact"><i class="fa fa-user"></i> Contacts</a></li>
                </ul>
            </li>
            <li>
                <a data-toggle="collapse" href="#newMenu" aria-expanded="false" aria-controls="newMenu"><i class="fa fa-plus-circle"></i> New</a>
                <ul class="sidebar-nav-sub collapse" id="newMenu">
                    <li><a href="<?=base_url()?>company/create"><i class="fa fa-building-o"></i> Company</a></li>
                    <li><a href="<?=base_url()?>contact/create"><i class="fa fa-user"></i> Contact</a></li>
                </ul>
            </li>
            <li>
                <a href="<?=base_url()?>marks/"><i class="fa fa-bookmark"></i> Bookmarks</a>
            </li>
            <li>
                <a href="<?=base_url()?>tasklist/"><i class="fa fa-tasks"></i> Tasks</a>
            </li>
            <li>
                <a href="<?=base_url()?>settings/gmail"><i class="fa fa-envelope-square"></i> Inbox</a>
            </li>            
            <li class="visible-xs visible-sm visible-md">
                <a href="<?=base_url()?>logout"><i class="fa fa-sign-out"></i> Logout</a>
            </li>
        </ul>
    </div>