    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/jquery-2.1.3.min.js"></script>
    <script>(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)</script>
    <!-- Bootstrap Scripts & 3rd Party Libs -->
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap-switch.min.js"></script>
    <!-- End -->
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/caineCustomSelectPicker.min.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/lynx.crm.js"></script>

    <!-- Modal -->
    <div class="modal fade" id="crm_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

            </div>
        </div>
    </div>
    <script type="text/template" id="modal-template">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Loading…</h4>
        </div>
        <div class="modal-body"></div>
    </script>

</body>
</html>