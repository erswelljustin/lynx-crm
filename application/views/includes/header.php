<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lynx technik UK Ltd. | CRM</title>
    <!-- Bootstrap Styles & 3rd Party Libs -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-select.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-switch.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-datetimepicker.min.css" charset="utf-8"/>
    <!-- End -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/lynx.crm.css" charset="utf-8"/>
    <!-- JQuery $(function(){}); anywhere -->
    <script>(function(w,d,u){w.readyQ=[];w.bindReadyQ=[];function p(x,y){if(x=="ready"){w.bindReadyQ.push(y);}else{w.readyQ.push(x);}};var a={ready:p,bind:p};w.$=w.jQuery=function(f){if(f===d||f===u){return a}else{p(f)}}})(window,document)</script>
</head>