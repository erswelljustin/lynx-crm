
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="btn btn-default navbar-btn pull-left nav-btn-custom visible-xs" id="menu-toggle"><i class="fa fa-reorder"></i></button>
                            <div class="nav-img-custom visible-xs"></div>
                            <button type="button" class="btn btn-default navbar-btn pull-right nav-btn-custom visible-xs" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><i class="fa fa-cog"></i> </button>
                        </div>

                        <ul class="nav navbar-nav visible-xs">

                        </ul>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                            <ul class="nav navbar-nav">
								<? if(get_instance()->router->fetch_class() == 'Find'){ ?>
    								<li><a href="<?=base_url()?>find/company"><i class="fa fa-building-o"></i> Company</a></li>
    								<li><a href="<?=base_url()?>find/contact"><i class="fa fa-user"></i> Contact</a></li>
								<? }elseif(in_array(get_instance()->router->fetch_class(), array('Marks', 'Settings','TaskList'))){ ?>

								<? }else{ ?>
                                    <li><a href="#" id="saveEntity"><i class="fa fa-save"></i> Save</a></li>
    								<?if(!isset($only_save)){ ?>
                                        <li><a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>note/<?=$entity?>/<?=$entity_id?>"><i class="fa fa-pencil-square-o"></i> Note</a></li>
                                        <li><a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>communication/<?=$entity?>/<?=$entity_id?>"><i class="fa fa-comments-o"></i> Communication</a></li>
                                        <li><a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>task/<?=$entity?>/<?=$entity_id?>"><i class="fa fa-tasks"></i> Task</a></li>
                                        <li><a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>delete/<?=$entity?>/<?=$entity_id?>" class="lcrm-delete"><i class="fa fa-remove"></i> Delete</a></li>
    								<? } ?>
								<? } ?>
							</ul>

                            <ul class="nav navbar-nav navbar-right">
							<? if(!in_array(get_instance()->router->fetch_class(), array('Find','Marks','Settings','TaskList')) || isset($_GET['s'])) { ?>
								<li><a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>bookmark/<?=$entity?>?book_url=<?=urlencode($_SERVER['REQUEST_URI'])?>"><i class="fa fa-bookmark"></i> Bookmark</a></li>
							<? }?>
                                <li><a href="#" onClick="window.location.reload()"><i class="fa fa-refresh"></i> Refresh</a></li>
                                <li class="dropdown hidden-sm hidden-xs hidden-md">
                                    <a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <?=get_instance()->user->getName()?>
                                        <img src="<?=get_gravatar(get_instance()->user->getEmail(), 60)?>" class="img-circle special-img"/>
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?=base_url()?>settings">Settings</a></li>
                                        <li class="divider"></li>
                                        <li><a href="<?=base_url()?>logout">Logout</a></li>
                                    </ul>
                                </li>
                                <li><a href="#" onclick="launchFullScreen(document.documentElement);" id="startFullScreen"><i class="fa fa-expand"></i></a></li>
                                <li><a href="#" onclick="launchFullScreen(document.documentElement);" id="exitFullScreen" class="hidden"><i class="fa fa-compress"></i></a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
