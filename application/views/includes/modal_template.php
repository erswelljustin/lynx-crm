        <form method="post">
            <div class="modal-header">
                <? if(isset($buttons['close'])) { ?>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <? } ?>
                <h4 class="modal-title"><?=$title?></h4>
            </div>
            <div class="modal-body">
                <?=$body?>
            </div>
            <? if(isset($buttons) && is_array($buttons)) { ?>
            <div class="modal-footer">
                <? if(isset($buttons['delete'])) { ?>
                <a data-target="#crm_modal" data-model-reload class="btn btn-danger" href="<?=base_url().$buttons['delete']?>">Delete</a>
                <? } if(isset($buttons['close'])) { ?>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=($buttons['close'] !== true ? $buttons['close'] : 'Close')?></button>
                <? } if(isset($buttons['submit'])) { ?>
                <button type="submit" class="btn btn-primary"><?=($buttons['submit'] !== true ? $buttons['submit'] : 'Submit')?></button>
                <? } ?>
            </div>
            <? } ?>
        </form>