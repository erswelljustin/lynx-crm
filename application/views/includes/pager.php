<?
	$entity = $pagerEntity;

	if(!isset($current)){ $current = 0; }
	if(!isset($total)){ $total = 0; }
	if(!isset($counter)){ $counter = 10; }

	$url = base_url('find/'.$entity).'?';
	foreach($_GET as $k => $v){
		if($k == 'offset'){ continue; }
		$url .= $k.'='.urlencode($v).'&';
	}
	$url .= 'offset=___offset___';


	$display = $_button_goLeft = $_button_goRight = false;
	$_button_maxCount = 5;
	$_start = $_end= 0;

	if($total > $counter){
		$display = true;
		$_page_current = floor(($current / $counter) + 1);
		if($_page_current < 1){ $_page_current = 1; }
		$_page_last = floor(($total / $counter) + 1);
		$_start = $_page_current > 3 ? $_page_current-2 : 1;
		$_end = $_page_current <= 3 ? ($_page_last > 5 ? 5 : $_page_last) : ($_page_current+2 <= $_page_last ? $_page_current+2 : $_page_last);
		$_start = $_page_last > 5 && $_end - $_start < 5 ? $_end-4 : $_start;
		$_end = $total == ($_page_last-1) * $counter ? $_end-1 : $_end;
		if($_page_current > 1){ $_button_goLeft = $_page_current-1; }
		if($_end > $_page_current){ $_button_goRight = $_page_current+1; }
	}

	if($display){
?>
<p class="record-count"><?=$total?> Records</p>
<ul class="pagination pagination-sm">
	<? if($_button_goLeft){ ?>
	<li><a href="<?= str_replace('___offset___', 0, $url) ?>">First</a></li>
	<li><a href="<?=str_replace('___offset___', (($_page_current-1) * $counter - $counter), $url)?>">&laquo;</a></li>
	<? }else{ ?>
	<li class="disabled"><a href="javascript:void(0);">&laquo;</a></li>
	<? } ?>

	<? for($i=$_start;$i<=$_end;$i++){ ?>
	<li<?=($i==$_page_current?' class="active"':'')?>><a href="<?=str_replace('___offset___', (($i-1) * $counter), $url)?>"><?=$i?></a></li>
	<? } ?>

	<? if($_button_goRight){ ?>
	<li><a href="<?=str_replace('___offset___', (($_page_current-1) * $counter + $counter), $url)?>">&raquo;</a></li>
	<li><a href="<?= str_replace('___offset___', round($total / $counter * $counter - $counter), $url) ?>">Last</a></li>
	<? }else{ ?>
	<li class="disabled"><a href="javascript:void(0);">&raquo;</a></li>
	<? } ?>
</ul>
<? } ?>