<form method="post" class="form-horizontal" id="<?=$entity?>Form" autocomplete="off">
	<input type="hidden" name="action" value="process" />
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="form-group form-group-sm">
					<label class="col-sm-3 control-label">Company <a href="<?=($contact ? base_url().'company/'.$contact->cont_company_id : 'javascript:void(0);')?>" class="btn btn-primary btn-xs"><i class="fa fa-building-o"></i></a></label>
					<div class="col-sm-9">
						<select name="cont_company_id" id="cont_company_id" class="form-control selectpicker" data-live-search="true" data-ignore-append>
							<option value="">Choose one…</option>
							<? foreach($companyList as $cId => $cName){ ?>
								<option<?=(($contact && $contact ? $contact->cont_company_id : '') == $cId ? ' selected="selected"':'')?> value="<?=$cId?>"><?=ucwords($cName)?></option>
							<? } ?>
						</select>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_full_name" class="col-sm-3 control-label">Full Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_full_name" placeholder="Contact Name" value="<?=($contact ? trim($contact->cont_first_name.' '.$contact->cont_last_name) : '')?>" readonly>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_first_name" class="col-sm-3 control-label">First Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_first_name" name="cont_first_name" placeholder="First Name" value="<?=($contact ? $contact->cont_first_name : '')?>">
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_last_name" class="col-sm-3 control-label">Last Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_last_name" name="cont_last_name" placeholder="Last Name" value="<?=($contact ? $contact->cont_last_name : '')?>">
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_title" class="col-sm-3 control-label">Title</label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_title" name="cont_title" placeholder="Job Title" value="<?=($contact ? $contact->cont_title : '')?>">
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_salutation" class="col-sm-3 control-label">Salutation</label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_salutation" name="cont_salutation" placeholder="Salutation" value="<?=($contact ? $contact->cont_salutation : '')?>">
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_dear" class="col-sm-3 control-label">Dear</label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_dear" name="cont_dear" placeholder="Dear" value="<?=($contact ? $contact->cont_dear : '')?>">
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_department" class="col-sm-3 control-label">Department</label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_department" name="cont_department" placeholder="Department" value="<?=($contact ? $contact->cont_department : '')?>">
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="form-group form-group-sm">
					<label for="cont_default_address_1" class="col-sm-3 control-label">
						Default Address
						<button class="btn btn-warning btn-xs" id="contact_address_fill" type="button"><i class="fa fa-copy"></i></button>
					</label>
					<div class="col-sm-9 address-group">
						<input type="text" class="form-control input-sm" id="cont_default_address_1" name="cont_default_address_1" placeholder="Default address 1" value="<?=($contact ? $contact->cont_default_address_1 : '')?>">
						<input type="text" class="form-control input-sm" id="cont_default_address_2" name="cont_default_address_2" placeholder="Default address 2" value="<?=($contact ? $contact->cont_default_address_2 : '')?>">
						<input type="text" class="form-control input-sm" id="cont_default_address_3" name="cont_default_address_3" placeholder="Default address 3" value="<?=($contact ? $contact->cont_default_address_3 : '')?>">
						<input type="text" class="form-control input-sm" name="cont_default_address_city" id="cont_default_address_city" placeholder="City" value="<?=($contact ? $contact->cont_default_address_city : '')?>"/>
						<input type="text" class="form-control input-sm" name="cont_default_address_postcode" id="cont_default_address_postcode" placeholder="Post Code" value="<?=($contact ? $contact->cont_default_address_postcode : '')?>"/>
						<input type="text" name="cont_default_address_county" id="cont_default_address_county" class="form-control typeahead input-sm" placeholder="County" value="<?=($contact ? $contact->cont_default_address_county : '')?>"/>
						<input type="text" name="cont_default_address_country" id="cont_default_address_country" class="form-control input-sm" placeholder="Country" value="<?=($contact ? $contact->cont_default_address_country : '')?>"/>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_phone" class="col-sm-3 control-label">Phone <a href="<?=($contact ? 'tel:'.$contact->cont_phone : 'javascript:void(0);')?>" class="btn btn-default btn-xs"><i class="fa fa-phone"></i></a></label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_phone" name="cont_phone" placeholder="Phone" value="<?=($contact ? $contact->cont_phone : '')?>">
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_mobile" class="col-sm-3 control-label">Mobile <a href="<?=($contact ? 'tel:'.$contact->cont_mobile : 'javascript:void(0);')?>" class="btn btn-default btn-xs"><i class="fa fa-phone"></i></a></label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_mobile" name="cont_mobile" placeholder="Mobile Phone" value="<?=($contact ? $contact->cont_mobile : '')?>">
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="form-group form-group-sm">
					<label for="cont_alt_address_1" class="col-sm-3 control-label">
						Alternative Address
						<br><button class="btn btn-default btn-xs" id="activate_postcode_lookup" type="button"><i class="fa fa-search"></i></button>
					</label>
					<div class="col-sm-9 address-group">
						<div class="postcode_lookup_container panel panel-default" style="display: none">
							<div class="panel-body">
								<div id="postcode_lookup"></div>
							</div>
						</div>
						<input type="text" class="form-control input-sm" id="cont_alt_address_1" name="cont_alt_address_1" placeholder="alternative address 1" value="<?=($contact ? $contact->cont_alt_address_1 : '')?>">
						<input type="text" class="form-control input-sm" id="cont_alt_address_2" name="cont_alt_address_2" placeholder="alternative address 2" value="<?=($contact ? $contact->cont_alt_address_2 : '')?>">
						<input type="text" class="form-control input-sm" id="cont_alt_address_3" name="cont_alt_address_3" placeholder="alternative address 3" value="<?=($contact ? $contact->cont_alt_address_3 : '')?>">
						<input type="text" class="form-control input-sm" name="cont_alt_address_city" id="cont_alt_address_city" placeholder="City" value="<?=($contact ? $contact->cont_alt_address_city : '')?>"/>
						<input type="text" class="form-control input-sm" name="cont_alt_address_postcode" id="cont_alt_address_postcode" placeholder="Post Code" value="<?=($contact ? $contact->cont_alt_address_postcode : '')?>"/>
						<input type="text" name="cont_alt_address_county" id="cont_alt_address_county" class="form-control typeahead input-sm" placeholder="County" value="<?=($contact ? $contact->cont_alt_address_county : '')?>"/>
						<input type="text" name="cont_alt_address_country" id="cont_alt_address_country" class="form-control input-sm" placeholder="Country" value="<?=($contact ? $contact->cont_alt_address_country : '')?>"/>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_email" class="col-sm-3 control-label">Email <a href="<?=($contact ? 'mailto:'.$contact->cont_email : 'javascript:void(0);')?>" class="btn btn-default btn-xs"><i class="fa fa-envelope-o"></i></a></label>
					<div class="col-sm-9">
						<input type="email" class="form-control input-sm" id="cont_email" name="cont_email" placeholder="Email Address" value="<?=($contact ? $contact->cont_email : '')?>">
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="cont_gender" class="col-sm-3 control-label">Gender</label>
					<div class="col-sm-9">
						<input type="text" class="form-control input-sm" id="cont_gender" name="cont_gender" placeholder="Gender" value="<?=($contact ? $contact->cont_gender : '')?>">
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
<? if ($entity_id){ ?>	
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div role="tabpanel">

					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation"><a data-tab-set-active href="#Contacts" aria-controls="Contacts" role="tab" data-toggle="tab">Other Contacts in Company</a></li>
						<li role="presentation"><a href="#Notes" aria-controls="Notes" role="tab" data-toggle="tab">Notes</a></li>
						<li role="presentation"><a href="#Communications" aria-controls="Communications" role="tab" data-toggle="tab">Comms</a></li>
						<li role="presentation"><a href="#Tasks" aria-controls="Tasks" role="tab" data-toggle="tab">Tasks</a></li>
						<li role="presentation"><a href="#Gmail" aria-controls="Gmail" role="tab" data-toggle="tab">EMail</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade active" id="Contacts">
							<div class="row">
								<div class="col-lg-12">
									<?=View::factory('partials/contacts/contact_list', array('rows'=>$contactList))?>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane fade in" id="Notes">
							<div class="row">
								<div class="col-lg-12">
									<?=View::factory('partials/notes/note_list', array('rows'=>$noteList))?>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="Communications">
							<div class="row">
								<div class="col-lg-12">
									<?=View::factory('partials/comms/comms_list', array('rows'=>$commList))?>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="Tasks">
							<div class="row">
								<div class="col-lg-12">
									<?=View::factory('partials/tasks/task_list_entity', array('rows'=>$taskList))?>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="Gmail">
							<div class="row">
								<div class="col-lg-12">
									<?=View::factory('partials/gmail/gmail_list')?>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
<? } ?>
</form>
