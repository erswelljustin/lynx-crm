<form method="post">
   <div class="container-fluid">
   		<div class="row">
   			<div class="col-xs-12">
				<? if(!is_null($error)){ ?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Error!</strong> <?=$error?>
				</div>
				<? }elseif(isset($success)){ ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Success!</strong> You have updated your password
				</div>
				<? } ?>
   			</div>	
   		</div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">			
				<div class="form-group">
					<label>Current password</label>
					<input type="password" name="pw[current]" class="form-control" />
				</div>
				<div class="form-group">
					<label>New password</label>
					<input type="password" name="pw[new]" class="form-control" />
				</div>
				<div class="form-group">
					<label>Repeat new password</label>
					<input type="password" name="pw[new_repeat]" class="form-control" />
				</div>
				<div class="form-group">
					<input type="hidden" name="action" value="process" />
					<button type="submit" class="btn btn-default btn-small">Update</button>
				</div>

			</div>
		</div>
	</div>
</form>