<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="table-responsive" data-partial-gmail-list>
				<table class="table table-striped">
				  <thead>
						<tr>
							<th width="5%"></th>
							<th width="2%"><i class="fa fa-circle"></i></th>
							<th width="2%"><i class="fa fa-flag"></i></th>
							<th width="20%">From</th>
							<th width="56%">Subject</th>
							<th width="10%">Date Received</th>
							<th width="5%" class="text-right">
								<a data-toggle="modal" data-target="#crm_modal" href="javascript: void(0);" class="btn btn-default btn-sm"><i class="fa fa-envelope-square"></i> New</a>
							</th>
						</tr>
					</thead>
					<tbody>
					<? foreach($google->listUsersMessages('To:me') as $row){ ?>
						<tr>
							<td>
								<a href="javascript: void(0);" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
							</td>
							<td class="<?=($row->read?'':'text-primary')?>"><?=($row->read ? ' ' : '<i class="fa fa-circle"></i>')?></td>
							<td class="<?=($row->flag?'text-danger':'')?>"><?=($row->flag ? '<i class="fa fa-flag"></i>' : ' ')?></td>
							<td class="gmail-from"><?=str_replace('"', '', $row->from)?></td>
							<td><?=$row->subject?></td>
							<td><?=date('d/m/Y H:i', strtotime($row->date))?></td>
							<td></td>
						</tr>
					<? } ?>		
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>