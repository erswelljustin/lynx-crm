<form method="post" class="form-horizontal" id="<?=$entity?>Form">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group form-group-sm">
					<label for="find_comp_name" class="col-sm-3 control-label">Company</label>
					<div class="col-sm-7">
						<select name="find[company]" id="find_comp_name" class="form-control selectpicker" data-live-search="true">
							<option value="">Choose one…</option>
							<? foreach($lists->company as $cId => $cName){ ?>
								<option<?=($form->company == $cId ? ' selected="selected"':'')?> value="<?=$cId?>"><?=ucwords($cName)?></option>
							<? } ?>
						</select>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="find_cont_first_name" class="col-sm-3 control-label">First Name</label>
					<div class="col-sm-7">
						<input type="text" class="form-control input-sm" id="find_cont_first_name" name="find[first_name]" placeholder="First Name" value="<?=$form->first_name?>" />
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="find_cont_last_name" class="col-sm-3 control-label">Last Name</label>
					<div class="col-sm-7">
						<input type="text" class="form-control input-sm" id="find_cont_last_name" name="find[last_name]" placeholder="Last Name" value="<?=$form->last_name?>"/>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="find_cont_department" class="col-sm-3 control-label">Department</label>
					<div class="col-sm-7">
						<input type="text" class="form-control input-sm" id="find_cont_department" name="find[department]" placeholder="Department" value="<?=$form->department?>"/>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="find_cont_location" class="col-sm-3 control-label">Location</label>
					<div class="col-sm-7">
						<input type="text" class="form-control input-sm" id="find_cont_location" name="find[location]" placeholder="Location" value="<?=$form->location?>"/>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<div class="col-sm-7 col-sm-offset-3 text-right">
						<input type="hidden" name="action" value="process" />
						<button type="submit" id="find_submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Search</button>
						<? if($processed){ ?>
							<a href="?" id="find_reset" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> Reset</a>
						<? } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<hr/>
		</div>
		<div class="col-sm-12">
			<div class="table-responsive">
				<div class="text-center"><?=$pager?></div>
				<? if(count($results)){ ?>
					<table class="table table-condensed table-striped">
						<thead>
						<th></th>
						<th>Contact Name</th>
						<th>Phone</th>
						<th>Mobile</th>
						<th>Email</th>
						<th>Department</th>
						<th colspan="2">Location</th>
						<th></th>
						</thead>
						<tbody>
						<? foreach($results as $row){ ?>
							<tr>
								<td><a href="<?=base_url()?>contact/<?=$row->cont_contact_id?>" class="btn btn-primary btn-xs"><i class="fa fa-arrow-circle-o-right"></i></a></td>
								<td><?=$row->cont_first_name.' '.$row->cont_last_name?></td>
								<td><? if(!empty($row->cont_phone)){ ?><a href="tel:<?=$row->cont_phone?>"><i class="fa fa-phone"></i> <?=$row->cont_phone?></a><? } ?></td>
								<td><? if(!empty($row->cont_mobile)){ ?><a href="tel:<?=$row->cont_mobile?>"><i class="fa fa-mobile"></i> <?=$row->cont_mobile?></a><? } ?></td>
								<td><? if(!empty($row->cont_email)){ ?><a href="mailto://<?=$row->cont_email?>"><i class="fa fa-envelope-o"></i> <?=$row->cont_email?></a><? } ?></td>
								<td><?=$row->cont_department?></td>
								<td><?=$row->cont_location?></td>
								<td><a href="<?=base_url()?>company/<?=$row->cont_company_id?>"><?=$row->company?></a></td>
								<td>
									<a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>bookmark/<?=$entity?>?book_url=<?=$entity.'/'.$row->cont_contact_id?>" class="btn btn-default btn-xs"><i class="fa fa-bookmark"></i></a>
								</td>
							</tr>
						<? } ?>
						</tbody>
					</table>
				<? }else{ ?>
					<div class="alert alert-warning">No results</div>
				<? } ?>
			</div>
		</div>
	</div>
</div>