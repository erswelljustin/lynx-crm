<form method="post" class="form-horizontal" id="<?=$entity?>Form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group form-group-sm">
                    <label for="find_comp_name" class="col-sm-3 control-label">Company Name</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" id="find_comp_name" name="find[name]" placeholder="Company Name" value="<?=$form->name?>" />
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="find_comp_alt_name" class="col-sm-3 control-label">Alt Name</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" id="find_comp_alt_name" name="find[alt_name]" placeholder="Alt Company Name" value="<?=$form->alt_name?>"/>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="find_comp_type" class="col-sm-3 control-label">Type</label>
                    <div class="col-sm-7">
                        <select name="find[type]" id="find_comp_type" class="form-control selectpicker" data-live-search="true">
														<option value="">Choose one…</option>
														<? foreach($lists->comp_type as $value){ ?>
															<option<?=($form->type == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
														<? } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="find_comp_default_address_postcode" class="col-sm-3 control-label">Default Postcode</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" id="find_comp_default_address_postcode" name="find[default_postcode]" placeholder="Default Company Address PostCode" value="<?=$form->default_postcode?>"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group form-group-sm">
                    <label for="find_comp_status" class="col-sm-3 control-label">Status</label>
                    <div class="col-sm-7">
                        <select name="find[status]" id="find_comp_status" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($lists->comp_status as $value){ ?>
														<option<?=($form->status == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="find_comp_source" class="col-sm-3 control-label">Source</label>
                    <div class="col-sm-7">
                        <select name="find[source]" id="find_comp_source" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($lists->comp_source as $value){ ?>
														<option<?=($form->source == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="find_comp_channel" class="col-sm-3 control-label">Channel</label>
                    <div class="col-sm-7">
                        <select name="find[channel]" id="find_comp_channel" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($lists->comp_channel as $value){ ?>
														<option<?=($form->channel == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
												</select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="find_comp_sector" class="col-sm-3 control-label">Sector</label>
                    <div class="col-sm-7">
                        <select name="find[sector]" id="find_comp_sector" class="form-control selectpicker" data-live-search="true">
													<option value="">Choose one…</option>
													<? foreach($lists->comp_sector as $value){ ?>
													<option<?=($form->sector == $value ? ' selected="selected"':'')?> value="<?=$value?>"><?=ucwords($value)?></option>
													<? } ?>
                        </select>
                    </div>
                </div>
								<div class="form-group form-group-sm">
									<div class="col-sm-7 col-sm-offset-3 text-right">
										<input type="hidden" name="action" value="process" />
										<button type="submit" id="find_submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Search</button>
										<? if($processed){ ?>
										<a href="?" id="find_reset" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> Reset</a>
										<? } ?>
									</div>
								</div>
            </div>
        </div>
    </div>
</form>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <hr/>
        </div>
        <div class="col-sm-12">
            <div class="table-responsive">
							<div class="text-center"><?=$pager?></div>
							<? if(count($results)){ ?>
                <table class="table table-condensed table-striped">
                    <thead>
                        <th></th>
                        <th>Company Name</th>
                        <th colspan="2">Address</th>
                        <th>Phone</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Channel</th>
                        <th>Sector</th>
                        <th></th>
                    </thead>
                    <tbody>
										<? foreach($results as $row){ ?>
                        <tr>
                            <td><a href="<?=base_url()?>company/<?=$row->comp_company_id?>" class="btn btn-primary btn-xs"><i class="fa fa-arrow-circle-o-right"></i></a></td>
                            <td><?=$row->comp_name?></td>
                            <td><?=$row->comp_default_address_1. (trim($row->comp_default_address_1) ? ', ' : '').$row->comp_default_address_city?></td>
                            <td><?=$row->comp_default_address_postcode?></td>
                            <td><? if(!empty($row->comp_phone)){ ?><a href="tel:<?=$row->comp_phone?>"><i class="fa fa-phone"></i> <?=$row->comp_phone?></a><? } ?></td>
                            <td><?=$row->comp_type?></td>
                            <td><?=$row->comp_status?></td>
                            <td><?=$row->comp_channel?></td>
                            <td><?=$row->comp_sector?></td>
                            <td>
                                <a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>bookmark/<?=$entity?>?book_url=<?=$entity.'/'.$row->comp_company_id?>" class="btn btn-default btn-xs"><i class="fa fa-bookmark"></i></a>
                            </td>
                        </tr>
										<? } ?>
                    </tbody>
                </table>
							<? }else{ ?>
									<div class="alert alert-warning">No results</div>
							<? } ?>
            </div>
        </div>
    </div>
</div>