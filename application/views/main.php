<body>
    <div id="wrapper">
        <?php $this->load->view('includes/sidebar');?>
        <div id="page-content-wrapper">
            <div class="page-content">
                <!-- Top Navbar -->
                <?php $this->load->view('includes/nav');?>
                <form method="post" class="form-horizontal" id="<?=$entity?>Form">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group form-group-sm">
                                    <label for="comp_name" class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control input-sm" id="comp_name" name="comp_name" placeholder="Company Name">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_name_alt" class="col-sm-3 control-label">Alt Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control input-sm" id="comp_name_alt" name="comp_name_alt" placeholder="Company Name">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_type" class="col-sm-3 control-label">Type</label>
                                    <div class="col-sm-9">
                                        <select name="comp_type" id="comp_type" class="form-control selectpicker" data-live-search="true">
                                            <option value="charity">Charity</option>
                                            <option value="consultant">Consultant</option>
                                            <option value="distributor">Distributor</option>
                                            <option value="end user">End User</option>
                                            <option value="independent contractor">Independent Contractor</option>
                                            <option value="manufacturer">Manufacturer</option>
                                            <option value="other">Other</option>
                                            <option value="systems integrator">Systems Integrator</option>
                                            <option value="reseller">Reseller</option>
                                            <option value="user">User</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_status" class="col-sm-3 control-label">Status</label>
                                    <div class="col-sm-9">
                                        <select name="comp_status" id="comp_status" class="form-control selectpicker" data-live-search="true">
                                            <option value="client">Client</option>
                                            <option value="competitor">Competitor</option>
                                            <option value="no longer trading">No Longer Trading</option>
                                            <option value="prospect">Prospect</option>
                                            <option value="supplier">Supplier</option>
                                            <option value="user">User</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_source" class="col-sm-3 control-label">Source</label>
                                    <div class="col-sm-9">
                                        <select name="comp_source" id="comp_source" class="form-control selectpicker" data-live-search="true">
                                            <option value="bve 2013">BVE 2013</option>
                                            <option value="competitor">Competitor</option>
                                            <option value="no longer trading">No Longer Trading</option>
                                            <option value="prospect">Prospect</option>
                                            <option value="supplier">Supplier</option>
                                            <option value="user">User</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_channel" class="col-sm-3 control-label">Channel</label>
                                    <div class="col-sm-9">
                                        <select name="comp_channel" id="comp_channel" class="form-control selectpicker" data-live-search="true">
                                            <option value="3D Broadcast">3D Broadcast</option>
                                            <option value="Altered Images">Altered Images</option>
                                            <option value="ATG">ATG</option>
                                            <option value="BSC">BSC</option>
                                            <option value="BVS">BVS</option>
                                            <option value="Canford">Canford</option>
                                            <option value="D+P MultiMedia">D+P MultiMedia</option>
                                            <option value="Digital Garage">Digital Garage</option>
                                            <option value="Direct">Direct</option>
                                            <option value="Gearhouse">Gearhouse</option>
                                            <option value="Get it Wired">Get it Wired</option>
                                            <option value="Matek">Matek</option>
                                            <option value="proAV">proAV</option>
                                            <option value="Production Gear">Production Gear</option>
                                            <option value="RCB Logic">RCB Logic</option>
                                            <option value="Thameside">Thameside</option>
                                            <option value="Tyrell">Tyrell</option>
                                            <option value="Wire Broadcast">Wire Broadcast</option>
                                            <option value="Wire Broadcast">Wire Broadcast</option>
                                            <option value="WTS">WTS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_sector" class="col-sm-3 control-label">Sector</label>
                                    <div class="col-sm-9">
                                        <select name="comp_sector" id="comp_sector" class="form-control selectpicker" data-live-search="true">
                                            <option value="Audio">Audio</option>
                                            <option value="Audio Visual">Audio Visual</option>
                                            <option value="Broadcast">Broadcast</option>
                                            <option value="Broadcaster">Broadcaster</option>
                                            <option value="Education">Education</option>
                                            <option value="Financial Services">Financial Services</option>
                                            <option value="Government">Government</option>
                                            <option value="Hire & Presentation">Hire & Presentation</option>
                                            <option value="IT & Telecommunications">IT & Telecommunications</option>
                                            <option value="Manufacturing">Manufacturing</option>
                                            <option value="Marketing & Advertising">Marketing & Advertising</option>
                                            <option value="Medical">Medical</option>
                                            <option value="Other">Other</option>
                                            <option value="Outside Broadcast">Outside Broadcast</option>
                                            <option value="Post Production">Post Production</option>
                                            <option value="Production">Production</option>
                                            <option value="Reseller">Reseller</option>
                                            <option value="Systems Integration">Systems Integration</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group form-group-sm">
                                    <label for="comp_default_address_1" class="col-sm-3 control-label">Default Address</label>
                                    <div class="col-sm-9 address-group">
                                        <input type="text" class="form-control input-sm" id="comp_default_address_1" name="comp_default_address_1" placeholder="Default address 1">
                                        <input type="text" class="form-control input-sm" id="comp_default_address_2" name="comp_default_address_2" placeholder="Default address 2">
                                        <input type="text" class="form-control input-sm" id="comp_default_address_3" name="comp_default_address_3" placeholder="Default address 3">
                                        <input type="text" class="form-control input-sm" name="comp_default_address_city" id="comp_default_address_city" placeholder="City"/>
                                        <input type="text" class="form-control input-sm" name="comp_default_address_postcode" id="comp_default_address_postcode" placeholder="Post Code"/>
                                        <input type="text" name="comp_default_address_county" id="comp_default_address_county" class="form-control typeahead" placeholder="County"/>
                                            <input type="text" name="comp_default_address_country" id="comp_default_address_country" class="form-control" placeholder="Country"/>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_primary_phone" class="col-sm-3 control-label">Phone</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control input-sm" id="comp_primary_phone" name="comp_primary_phone" placeholder="Primary Phone">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_fax" class="col-sm-3 control-label">Fax</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control input-sm" id="comp_fax" name="comp_fax" placeholder="Fax">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group form-group-sm">
                                    <label for="comp_invoice_address_1" class="col-sm-3 control-label">Invoice Address</label>
                                    <div class="col-sm-9 address-group">
                                        <input type="text" class="form-control input-sm" id="comp_invoice_address_1" name="comp_invoice_address_1" placeholder="invoice address 1">
                                        <input type="text" class="form-control input-sm" id="comp_invoice_address_2" name="comp_invoice_address_2" placeholder="invoice address 2">
                                        <input type="text" class="form-control input-sm" id="comp_invoice_address_3" name="comp_invoice_address_3" placeholder="invoice address 3">
                                        <input type="text" class="form-control input-sm" name="comp_invoice_address_city" id="comp_invoice_address_city" placeholder="City"/>
                                            <input type="text" class="form-control input-sm" name="comp_invoice_address_postcode" id="comp_invoice_address_postcode" placeholder="Post Code"/>
                                        <input type="text" name="comp_invoice_address_county" id="comp_invoice_address_county" class="form-control typeahead" placeholder="County"/>
                                        <input type="text" name="comp_invoice_address_country" id="comp_invoice_address_country" class="form-control" placeholder="Country"/>
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_email" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control input-sm" id="comp_email" name="comp_email" placeholder="Email Address">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label for="comp_website" class="col-sm-3 control-label">Website</label>
                                    <div class="col-sm-9">
                                        <input type="url" class="form-control input-sm" id="comp_website" name="comp_website" placeholder="Website">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Credit Information
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-sm">
                                                    <label for="comp_currency" class="col-sm-3 control-label">Currency</label>
                                                    <div class="col-sm-9">
                                                        <select name="comp_currency" id="comp_currency" class="form-control selectpicker">
                                                            <option value="gbp" data-icon="fa fa-gbp">Stirling</option>
                                                            <option value="euro" data-icon="fa fa-euro">Euro</option>
                                                            <option value="dollar" data-icon="fa fa-dollar">Dollar</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-sm">
                                                    <label for="comp_credit_terms" class="col-sm-3 control-label">Terms</label>
                                                    <div class="col-sm-9">
                                                        <input type="checkbox" name="comp_credit_terms" id="comp_credit_terms" class="bs-select"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-sm">
                                                    <label for="comp_sage_id" class="col-sm-3 control-label">Sage #</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="comp_sage_id" id="comp_sage_id" class="form-control input-sm"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-sm">
                                                    <label for="comp_discount_5k" class="col-sm-3 control-label">Disc 5K</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="comp_discount_5k" id="comp_discount_5k" class="form-control input-sm"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group form-group-sm">
                                                    <label for="comp_discount_yb" class="col-sm-3 control-label">Disc YB</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="comp_discount_yb" id="comp_discount_yb" class="form-control input-sm"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div role="tabpanel">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a data-tab-set-active href="#Contacts" aria-controls="Contacts" role="tab" data-toggle="tab">Contacts</a></li>
                                    <li role="presentation"><a href="#Notes" aria-controls="Notes" role="tab" data-toggle="tab">Notes</a></li>
                                    <li role="presentation"><a href="#Communications" aria-controls="Communications" role="tab" data-toggle="tab">Comms</a></li>
                                    <li role="presentation"><a href="#Tasks" aria-controls="Tasks" role="tab" data-toggle="tab">Tasks</a></li>
<!--                                    <li role="presentation"><a href="#Appointments" aria-controls="Appointments" role="tab" data-toggle="tab">Appointments</a></li>-->
<!--                                    <li role="presentation"><a href="#Quotes" aria-controls="Quotes" role="tab" data-toggle="tab">Quotes</a></li>-->
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active" id="Contacts">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <?php $this->load->view('partials/contacts/contact_list'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade in" id="Notes">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <?php $this->load->view('partials/notes/note_list'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="Communications">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <?php $this->load->view('partials/comms/comms_list'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="Tasks">...</div>
<!--                                    <div role="tabpanel" class="tab-pane fade" id="Appointments">...</div>-->
<!--                                    <div role="tabpanel" class="tab-pane fade" id="Quotes">...</div>-->
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function(){
            var saveButton = $('#saveEntity'),
                <?=$entity?>Form = $('#<?=$entity?>Form');

            saveButton.bind('click', function(){
                $.post(
                    '<?=base_url().$entity?>/save/<?=$entity_id?>',
                    <?=$entity?>Form.serializeObject(),
                    function(response) {
                        console.log(response);
                    },
                    'json'
                );
            });

        });
    </script>