<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Lynx technik UK Ltd. | CRM</title>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome.min.css" charset="utf-8"/>
    <link rel="stylesheet" href="<?=base_url();?>assets/css/lynx.crm.login.css" charset="utf-8"/>
</head>
<body>
    <div class="container">
        <div id="login-wraper">
            <form class="form-horizontal login-form" method="post">
                <legend>Login to <span class="ffc-red">Lynx CRM</span></legend>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" name="action" class="btn btn-default">Sign in</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" charset="UTF-8" src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
    
    <? if($error){ ?>
    <script type="text/javascript"> alert('<?=$error?>'); </script>
    <? } ?>
</body>
</html>