<div class="table-responsive" data-partial-comm-list>
    <table class="table table-striped">
        <thead>
				<tr>
					<th></th>
					<th>Date</th>
					<th>Title</th>
					<th>Type</th>
					<th>User</th>
					<th>Content</th>
					<th class="text-right">
							<a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>communication/<?=$entity?>/<?=$entity_id?>" class="btn btn-default btn-sm"><i class="fa fa-comments-o"></i> Communication</a>
					</th>
				</tr>
        </thead>
				<tbody>
				<? foreach($rows as $comm) { ?>
					<tr>
						<td><a data-toggle="modal" data-target="#crm_modal" class="btn btn-primary btn-xs" href="<?=base_url()?>communication/<?=$entity?>/<?=$entity_id?>?id=<?=$comm->comm_id?>"><i class="fa fa-pencil-square-o"></i> Edit</a></td>
						<td><?=$comm->comm_date?></td>
						<td><?=$comm->comm_title?></td>
						<td><?=get_instance()->communication->getConfiguration('type')[$comm->comm_type]?></td>
						<td><?=get_instance()->user->get($comm->comm_user_id)->user_name?></td>
						<td><?=$comm->comm_text?></td>
						<td class="text-right">
							<a data-toggle="modal" data-target="#crm_modal" class="btn btn-danger btn-xs" href="<?=base_url()?>communication/<?=$entity?>/<?=$entity_id?>?id=<?=$comm->comm_id?>&confirm"><i class="fa fa-pencil-square-o"></i> Delete</a>
						</td>
					</tr>
				<? } ?>
				</tbody>
    </table>
</div>