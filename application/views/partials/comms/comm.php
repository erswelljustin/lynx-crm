<div data-modal-form="<?=base_url()?>communication/<?=$entity?>/<?=$entity_id?><?=(isset($form->comm_id)?'?id='.$form->comm_id:'')?>">
    <div class="form-group">
        <label for="comm_title">Title</label>
        <input type="text" name="comm_title" class="form-control" value="<?=$form->comm_title?>"/>
    </div>
    <div class="form-group">
        <label for="comm_type">Type</label>
        <select name="comm_type" id="comm_type" class="form-control selectpicker" data-live-search="true">
					<option value="">Choose one…</option>
					<? foreach(get_instance()->communication->getConfiguration('type') as $type_id => $type_value){ ?>
						<option value="<?= $type_id ?>"<?=($form && $form->comm_type == $type_id ? ' selected="selected"':'')?>><?=$type_value?></option>
					<? } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="comm_date">Date</label>
        <input type="text" name="comm_date" class="form-control datetimepicker" value="<?=($form->comm_date ? date('d/m/Y g:i A',$form->comm_date): '' )?>"/>
    </div>
    <div class="form-group">
        <label for="comm_text">Text</label>
        <textarea name="comm_text" class="form-control"><?=$form->comm_text?></textarea>
    </div>
</div>