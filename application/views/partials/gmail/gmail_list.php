<div class="table-responsive" data-partial-gmail-list>
	<table class="table table-striped">
	  <thead>
			<tr>
				<th width="5%"></th>
				<th width="2%"><i class="fa fa-circle"></i></th>
				<th width="2%"><i class="fa fa-flag"></i></th>
				<th width="20%">From</th>
				<th width="56%">Subject</th>
				<th width="10%">Date Received</th>
				<th width="5%" class="text-right">
					<a data-toggle="modal" data-target="#crm_modal" href="javascript: void(0);" class="btn btn-default btn-sm"><i class="fa fa-envelope-square"></i> New</a>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<a href="javascript: void(0);" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
				</td>
				<td class="text-primary"><i class="fa fa-circle"></i></td>
				<td class="text-danger"><i class="fa fa-flag"></i></td>
				<td>Apple</td>
				<td>Thanks for contacting Apple Support.</td>
				<td>Today 15:12</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<a href="javascript: void(0);" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
				</td>
				<td><i class="fa fa-circle-o"></i></td>
				<td></td>
				<td>Apple</td>
				<td>Thanks for contacting Apple Support.<span class="pull-right text-muted"><strong>Thread</strong>: 2 Messages</span></td>
				<td>Today 14:44</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<a href="javascript: void(0);" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
				</td>
				<td><i class="fa fa-circle-o"></i></td>
				<td></td>
				<td>Apple</td>
				<td>Thanks for contacting Apple Support.<span class="pull-right text-muted"><strong>Thread</strong>: 2 Messages</span></td>
				<td>Today 14:44</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<a href="javascript: void(0);" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
				</td>
				<td><i class="fa fa-circle-o"></i></td>
				<td></td>
				<td>Apple</td>
				<td>Thanks for contacting Apple Support.<span class="pull-right text-muted"><strong>Thread</strong>: 2 Messages</span></td>
				<td>Today 14:44</td>
				<td></td>
			</tr>
		</tbody>
	</table>
</div>