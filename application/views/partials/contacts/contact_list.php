<div class="table-responsive">
    <table class="table table-striped">
        <thead>
		<th></th>
        <th>Name</th>
        <th>Title</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Mobile</th>
        <th class="text-right">
<!-- 		<? if(get_instance()->router->fetch_class() == 'Company'){ ?>
    	    <a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>contact/<?=$entity?>/<?=$entity_id?>" class="btn btn-default btn-sm"><i class="fa fa-user"></i> Contact</a>
		<? } ?> -->
		</th>
        </thead>
		<tbody>
			<?php foreach($rows as $contact){?>
				<tr>
					<td>
						<a href="<?=base_url()?>contact/<?=$contact->cont_contact_id?>" class="btn btn-primary btn-xs"><i class="fa fa-arrow-circle-o-right"></i></a>
					</td>
					<td><?=$contact->cont_first_name.' '.$contact->cont_last_name?></td>
					<td><?=$contact->cont_title?></td>
					<td><?=$contact->cont_email?></td>
					<td><?=$contact->cont_phone?></td>
					<td colspan="2"><?=$contact->cont_mobile?></td>
				</tr>
			<? } ?>
		</tbody>
    </table>
</div>