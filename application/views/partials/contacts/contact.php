<div data-modal-form="<?=base_url()?>contact/<?=$entity?>/<?=$entity_id?>">
    <div class="form-group">
        <label for="cont_first_name">First Name</label>
        <input type="text" name="cont_first_name" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="cont_last_name">Last Name</label>
        <input type="text" name="cont_last_name" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="cont_job_title">Last Name</label>
        <input type="text" name="cont_last_name" class="form-control"/>
    </div>

    <div class="form-group">
        <label for="comm_type">Type</label>
        <select name="comm_type" id="comm_type" class="form-control selectpicker" data-live-search="true">
            <option value="email">Email</option>
            <option value="phone">Phone Call</option>
            <option value="fax">Fax</option>
            <option value="skype">Skype</option>
            <option value="other">Other</option>
        </select>
    </div>
    <div class="form-group">
        <label for="comm_date">Date</label>
        <input type="date" name="comm_date" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="comm_user">User</label>
        <input type="text" name="comm_user" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="comm_text">Text</label>
        <textarea name="comm_text" class="form-control"></textarea>
    </div>
</div>