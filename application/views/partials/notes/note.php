<div data-modal-form="<?=base_url()?>note/<?=$entity?>/<?=$entity_id?><?=(isset($form->note_id)?'?id='.$form->note_id:'')?>">
    <div class="form-group">
        <label for="note_title">Title</label>
        <input type="text" name="note_title" class="form-control" value="<?=$form->note_title?>" />
    </div>
    <div class="form-group">
        <label for="note_date">Date</label>
        <input type="text" name="note_date" class="form-control datetimepicker" value="<?=($form->note_date ? date('d/m/Y g:i A',$form->note_date): '' )?>" />
    </div>
    <div class="form-group">
        <label for="note_text">Text</label>
        <textarea name="note_text" class="form-control"><?=$form->note_text?></textarea>
    </div>
</div>