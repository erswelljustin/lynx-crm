<div class="table-responsive" data-partial-note-list>
    <table class="table table-striped">
        <thead>
					<tr>
						<th></th>
						<th>Date</th>
						<th>Title</th>
						<th>User</th>
						<th>Content</th>
						<th class="text-right" >
								<a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>note/<?=$entity?>/<?=$entity_id?>" class="btn btn-default btn-sm"><i class="fa fa-pencil-square-o"></i> Note</a>
						</th>
					</tr>
        </thead>
				<tbody>
					<? foreach($rows as $note){?>
						<tr>
							<td>
								<a data-toggle="modal" data-target="#crm_modal" class="btn btn-primary btn-xs" href="<?=base_url()?>note/<?=$entity?>/<?=$entity_id?>?id=<?=$note->note_id?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
							</td>
							<td><?=$note->note_date?></td>
							<td><?=$note->note_title?></td>
							<td><?=get_instance()->user->get($note->note_user_id)->user_name?></td>
							<td><?=$note->note_text?></td>
							<td class="text-right"><a data-toggle="modal" data-target="#crm_modal" class="btn btn-danger btn-xs" href="<?=base_url()?>note/<?=$entity?>/<?=$entity_id?>?id=<?=$note->note_id?>&confirm"><i class="fa fa-pencil-square-o"></i> Delete</a></td>
						</tr>
					<? } ?>
				</tbody>
    </table>
</div>