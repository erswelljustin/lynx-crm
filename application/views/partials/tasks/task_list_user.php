<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="text-muted">Your Tasks</h2>
			<div class="row">
			<?
				$blocks = array();
				foreach($results as $row1){
					if(!isset($blocks[$row1->task_entity])){
						$blocks[$row1->task_entity] = array();
					}
					$blocks[$row1->task_entity][] = $row1;
				}
				
				foreach($blocks as $e => $rows){
			?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<?=ucfirst($e)?>
							</div>
							<table class="table">
								<thead>
									<tr>
										<th>For</th>
										<th>Title</th>
										<th>Date</th>
										<th>Type</th>
										<th>Priority</th>
										<th>Status</th>
										<th>Text</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<? foreach($rows as $row){ 
										$type = get_instance()->task->getConfiguration('type')[$row->task_type];
										$priority = get_instance()->task->getConfiguration('priority')[$row->task_priority];
										$color = '';
										$status = get_instance()->task->getConfiguration('status')[$row->task_status];
									?>
										<tr>
											<td><a href="<?=base_url().$row->task_entity.'/'.$row->task_entity_id?>"><?=$row->task_entity_name;?></a></td>
											<td><?=$row->task_title;?></td>
											<td><?=date('d/m/Y H:i', strtotime($row->task_due));?></td>
											<td><?=$type;?></td>
											<td><?=$priority;?></td>
											<td><?=$status;?></td>
											<td><?=$row->task_text;?></td>
											<td class="text-right">
												<a data-toggle="modal" data-target="#crm_modal" class="btn btn-default btn-xs" href="<?=base_url()?>task/<?=$row->task_entity?>/<?=$row->task_entity_id?>?id=<?=$row->task_id?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
											</td>
										</tr>
									<? } ?>
								</tbody>
							</table>
						</div>
					</div>
				<?
				}
			?>
			</div>
		</div>
	</div>
</div>