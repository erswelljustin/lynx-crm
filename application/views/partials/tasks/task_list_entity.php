<div class="table-responsive" data-partial-task-list>
	<table class="table table-striped">
		<thead>
		<tr>
			<th></th>
			<th>Title</th>
			<th>Type</th>
			<th>Priority</th>
			<th>Due</th>
			<th>Status</th>
			<th>User</th>
			<th class="text-right">
				<a data-toggle="modal" data-target="#crm_modal" href="<?=base_url()?>task/<?=$entity?>/<?=$entity_id?>" class="btn btn-default btn-sm"><i class="fa fa-tasks"></i> Task</a>
			</th>
		</tr>
		</thead>
		<? foreach($rows as $task){?>
			<tr>
				<td>
					<a data-toggle="modal" data-target="#crm_modal" class="btn btn-primary btn-xs" href="<?=base_url()?>task/<?=$entity?>/<?=$entity_id?>?id=<?=$task->task_id?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
				</td>
				<td><?=$task->task_title?></td>
				<td><?=get_instance()->task->getConfiguration('type')[$task->task_type]?></td>
				<td><?=get_instance()->task->getConfiguration('priority')[$task->task_priority]?></td>
				<td><?=$task->task_due?></td>
				<td><?=get_instance()->task->getConfiguration('status')[$task->task_status]?></td>
				<td><?=get_instance()->user->get($task->task_user_id)->user_name?></td>
				<td class="text-right"><a data-toggle="modal" data-target="#crm_modal" class="btn btn-danger btn-xs" href="<?=base_url()?>task/<?=$entity?>/<?=$entity_id?>?id=<?=$task->task_id?>&confirm"><i class="fa fa-pencil-square-o"></i> Delete</a></td>
			</tr>
		<? } ?>
	</table>
</div>