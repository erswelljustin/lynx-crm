<div data-modal-form="<?=base_url()?>task/<?=$entity?>/<?=$entity_id?><?=(isset($form->task_id)?'?id='.$form->task_id:'')?>">
	<div class="form-group">
		<label for="task_title">Title</label>
		<input type="text" name="task_title" class="form-control" value="<?=$form->task_title?>"/>
	</div>
	<div class="form-group">
		<label for="task_due">Due Date</label>
		<input type="text" name="task_due" class="form-control datetimepicker" value="<?=($form->task_due ? date('d/m/Y g:i A',$form->task_due): '' )?>"/>
	</div>
	<div class="form-group">
		<label for=task_priority">Priority</label>
		<select name="task_priority" id="task_priority" class="form-control selectpicker">
			<option value="">Choose one…</option>
			<? foreach(get_instance()->task->getConfiguration('priority') as $type_id => $type_value){ ?>
				<option value="<?= $type_id ?>"<?=($form && $form->task_priority == $type_id ? ' selected="selected"':'')?>><?=$type_value?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="task_status">Status</label>
		<select name="task_status" id="task_status" class="form-control selectpicker">
			<option value="">Choose one…</option>
			<? foreach(get_instance()->task->getConfiguration('status') as $type_id => $type_value){ ?>
				<option value="<?= $type_id ?>"<?=($form && $form->task_status == $type_id ? ' selected="selected"':'')?>><?=$type_value?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="task_type">Type</label>
		<select name="task_type" id="task_type" class="form-control selectpicker">
			<option value="">Choose one…</option>
			<? foreach(get_instance()->task->getConfiguration('type') as $type_id => $type_value){ ?>
				<option value="<?= $type_id ?>"<?=($form && $form->task_type == $type_id ? ' selected="selected"':'')?>><?=$type_value?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="task_text">Text</label>
		<textarea name="task_text" class="form-control"><?=$form->task_text?></textarea>
	</div>
</div>