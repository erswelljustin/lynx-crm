<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="text-muted">Bookmarks</h2>
			<div class="row">
			<?
				$blocks = array();
				foreach($results as $row1){
					if(!isset($blocks[$row1->book_entity])){
						$blocks[$row1->book_entity] = array();
					}
					$blocks[$row1->book_entity][] = $row1;
				}
				
				foreach($blocks as $e => $rows){
			?>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<? if($e == 'find') { echo 'Searches'; } else { echo ucfirst($e); }?>
							</div>
							<table class="table">
								<thead>
									<tr>
										<th>Title</th>
										<th>Date</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<? foreach($rows as $row){ ?>
										<tr>
											<td><?=$row->book_title;?></td>
											<td><?=date('d/m/Y', strtotime($row->book_date));?></td>
											<td class="text-right">
												<a href="<?=base_url().ltrim($row->book_url, '/');?>" class="btn btn-primary btn-xs"><i class="fa fa-arrow-circle-o-right"></i> Go</a>
												<a data-toggle="modal" data-target="#crm_modal" class="btn btn-default btn-xs" href="<?=base_url()?>bookmark/<?=$row->book_entity?>?id=<?=$row->book_id?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
											</td>
										</tr>
									<? } ?>
								</tbody>
							</table>
						</div>
					</div>
				<?
				}
			?>
			</div>
		</div>
	</div>
</div>