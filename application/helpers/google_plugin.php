<?

	// Load Google SDK
	require_once dirname(__FILE__).'/../../vendor/autoload.php';

	// General functionality for the end user 
	class PluginGoogleFunctions extends Google_Client {

		private $_getUserInfo_holder = null;
		protected $_Google_Service_Gmail = null;
		protected $_oauth2 = null;

		public function getUserInfo(){
			if(is_null($this->_getUserInfo_holder)){
				$this->_getUserInfo_holder = $this->oauth2()->userinfo->get();
			}

			return $this->_getUserInfo_holder;
		}

		public function listUsersLabels(){
			$rows = array();
			$response = $this->gmail()->users_labels->listUsersLabels('me');
			foreach($response->getLabels() as $row){
				$rows[] = $row;
			}
			return $rows;
		}

		public function listUsersMessages($filter = ''){
			$rows = array();
			$params = array();

			if($filter){
				$params['q'] = $filter;
			}

			$params['maxResults'] = 20;

			$threads = array();
			$messages = $this->gmail()->users_messages->listUsersMessages('me', $params);
			if($msg = $messages->getMessages()){
				foreach($msg as $row){

					if(!isset($threads[$row->getThreadId()])){
						$threads[$row->getThreadId()] = 1;
					}else{
						$threads[$row->getThreadId()]++;
						continue;
					}

					$row = $this->gmail()->users_messages->get('me', $row->getId(),array('format'=>'metadata'));

					$entry = (object)array(
						'tid' => $row->getThreadId(),
						'date' => 'Unknown Date',
						'subject' => '(No Subject)',
						'from' => 'Unknown Sender',
						'sender' => array(),
						'flag' => false,
						'important' => false,
						'read' => true
					);

					foreach($row->getPayload()->getHeaders() as $header){
						if($header->getName() == 'Subject' && $header->getValue()){
							$entry->subject = $header->getValue();
						}elseif($header->getName() == 'From' && $header->getValue()){
							$entry->sender[] = $header->getValue();
						}elseif($header->getName() == 'Sender' && $header->getValue()){
							$entry->sender[] = $header->getValue();
						}elseif($header->getName() == 'Date'){
							$entry->date = $header->getValue();
						}

						if(count($entry->sender)){
							$entry->from = implode(' - ',$entry->sender);
						}

						$labels = $row->getLabelIds();
						// $l = '';
						foreach($labels as $label){
							// $l.=($l?' ':'').$label;
							if($label == 'UNREAD'){
								$entry->read = false;
							}
							if($label == 'IMPORTANT'){
								$entry->important = true;
							}
							if($label == 'STARRED'){
								$entry->flag = true;
							}
						}
						// $entry->subject = '('.$l.') '.$entry->subject;
					}

					$rows[] = $entry;
				}
			}

			foreach($rows as $index=>$row){
				if($threads[$row->tid] > 1){
					$rows[$index]->subject = $rows[$index]->subject.'<span class="pull-right text-muted"><strong>Thread</strong>: '.$threads[$row->tid].' messages</span>';
				}
			}

			return $rows;
		}

	}

	// General class for the plugin to extend the google client 
	// and gain extra functionality for the plugin
	class PluginGoogleLib extends PluginGoogleFunctions {

		// holds current db connection
		private static $_database_link = null;

		// main table to store access tokens
		protected $_table_name = 'crm_google_access';

		// rewrites google client
		public function setRedirectUri($uri){
			parent::setRedirectUri($uri);
			return $this;
		}

		// rewrites google client
		public function setAuthConfigFile($filename){
			parent::setAuthConfigFile($filename);
			return $this;
		}

		// rewrites google client
		public function addScope($scope){
			parent::addScope($scope);
			return $this;
		}

		// extends pdo class
		public static function __callStatic($name, $args){
			$callback = array(self::$_database_link, $name);
			return call_user_func_array($callback, $args);
		}

		// making redirects easy :p
		protected static function _redirect($url){
			$url = filter_var($url, FILTER_SANITIZE_URL);
			@header('Location: '.$url);
			echo '<script type="text/javascript"> window.location = \''.$url.'\'; </script>';
			exit;
		}

		// what?
		public function connectDatabase($engine, $host, $user, $pass, $database){
			try{
				$dns = $engine.':dbname='.$database.';host='.$host;

				$link = new Pdo($dns, $user, $pass, array(
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
					PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
				));

				self::$_database_link = $link;
			}catch(Exception $e){
				die('Plugin Google (Database Error): '.$e->getMessage());
			}

			return $this;
		}

		public function encrypt($str,$ky=''){
			if($ky==''){
				return $str;
			}

			$ky = str_replace(chr(32),'',$ky);
			if(strlen($ky)<8){
				exit('key error');
			}

			$kl=strlen($ky)<32?strlen($ky):32;
			$k=array();
			for($i=0;$i<$kl;$i++){
				$k[$i]=ord($ky{$i})&0x1F;
			}
			
			$j=0;
			for($i=0;$i<strlen($str);$i++){
				$e=ord($str{$i});
				$str{$i}=$e&0xE0?chr($e^$k[$j]):chr($e);
				$j++;$j=$j==$kl?0:$j;
			}

			return $str;
		}

	}

	// main functionality for the plugin
	class PluginGoogle extends PluginGoogleLib {

		// Authorise user
		public function authorise($auth_id){

			if(!isset($_GET['code'])){
				if($this->authorised($auth_id)){
					return $this;
				}

				$this
					->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY)
					->addScope(Google_Service_Gmail::GMAIL_READONLY)
					->addScope(Google_Service_Plus::USERINFO_EMAIL)
					->addScope(Google_Service_Plus::USERINFO_PROFILE);

				$auth_url = $this->createAuthUrl();
				self::_redirect($auth_url);
			}

			try{
				$this->authenticate($_GET['code']);
				$access_token = $this->getAccessToken();
				$this->setAccessToken($access_token);

				$db = self::prepare('REPLACE INTO `'.$this->_table_name.'` (auth_id,access_token,created) VALUES(:auth_id,:access_token,NOW())');
				$db->execute(array(
					':auth_id' => $auth_id,
					':access_token' => $this->encrypt($access_token, 'google-plugin-by-cool-dudes')
				));

				$get = $_GET;
				unset($get['code']);
				$query = '?';
				foreach($get as $a=>$b){
					$query .= $a.'='.urlencode($b).'&';
				}

				$query .= 'GOOGLE_PLUGIN_ALLOW_REDIRECT_AWAY=&';

				self::_redirect($query);
			}catch(Exception $e){
				die('Failed to aquire auth code');
			}
		}

		// Validate if user is authorised
		protected function authorised($auth_id){
			$db = self::prepare('SELECT access_token FROM `'.$this->_table_name.'` WHERE auth_id = :auth_id');
			$db->execute(array(
				':auth_id' => $auth_id
			));

			if(!$result = $db->fetch()){
				return false;
			}

			if(!$result->access_token){
				return false;
			}

			$result->access_token = $this->encrypt($result->access_token, 'google-plugin-by-cool-dudes');

			$success = false;
			try{
				$this->setAccessToken($result->access_token);
				$success = true;
			}catch(Exception $e){}

			if($this->isAccessTokenExpired()){
				return false;
			}

			return $success;
		}

		// easy access to google service (gmail)
		protected function gmail(){
			if(is_null($this->_Google_Service_Gmail)){
				$this->_Google_Service_Gmail = new Google_Service_Gmail($this);
			}

			return $this->_Google_Service_Gmail;
		}

		// easy access to oauth2
		protected function oauth2(){
			if(is_null($this->_oauth2)){
				$this->_oauth2 = new Google_Service_Oauth2($this);
			}
			return $this->_oauth2;
		}

	}
