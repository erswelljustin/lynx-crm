<?

	function google_plugin(){
		static $googlePlugin;

		if(!class_exists('PluginGoogle')){
			include dirname(__FILE__).'/google_plugin.php';

			$db = array();
			include dirname(__FILE__).'/../config/database.php';

			$googlePlugin = new PluginGoogle;
			$googlePlugin
				->connectDatabase(
					'mysql',
					$db[$active_group]['hostname'],
					$db[$active_group]['username'],
					$db[$active_group]['password'],
					$db[$active_group]['database'])
				->setAuthConfigFile(dirname(__FILE__).'/../config/creds.json')
				->setRedirectUri(base_url() . 'settings/google_plugin');
		}

		return $googlePlugin;

	}

	function _encrypt($str,$ky=''){
		if($ky==''){ return $str; }
		$ky=str_replace(chr(32),'',$ky);
		if(strlen($ky)<8){ exit('key error'); }
		$kl=strlen($ky)<32?strlen($ky):32;
		$k=array();
		for($i=0;$i<$kl;$i++){ $k[$i]=ord($ky{$i})&0x1F; }
		$j=0;
		for($i=0;$i<strlen($str);$i++){
			$e=ord($str{$i});
			$str{$i}=$e&0xE0?chr($e^$k[$j]):chr($e);
			$j++;$j=$j==$kl?0:$j;
		}
		return $str;
	}

	function _is_email($email){
		return !(preg_match("/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/", $email) || !preg_match("/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email));
	}

	function _is_valid_date_input($date){
		$d = explode('-', $date);
		$d[0] = abs(isset($d[0]) && is_numeric($d[0]) ? intval($d[0]) : 0);
		$d[1] = abs(isset($d[1]) && is_numeric($d[1]) ? intval($d[1]) : 0);
		$d[2] = abs(isset($d[2]) && is_numeric($d[2]) ? intval($d[2]) : 0);
		if(!$d[0] || !$d[1] || !$d[2] || $d[2] > 31 || $d[1] > 12){
			return false;
		}
		return date('Y-m-d', strtotime($date));
	}

	function debug(){
		$vars = func_get_args();
		echo '<pre>';
		call_user_func_array((count($vars) > 1 ? 'var_dump' : 'var_export'), $vars);
		echo '</pre>';
	}

	/**
	* Get either a Gravatar URL or complete image tag for a specified email address.
	*
	* @param string $email The email address
	* @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
	* @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
	* @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
	* @param boole $img True to return a complete IMG tag False for just the URL
	* @param array $atts Optional, additional key/value attributes to include in the IMG tag
	* @return String containing either just a URL or a complete image tag
	* @source http://gravatar.com/site/implement/images/php/
	*/
	function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
		$url = 'http://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
		    $url = '<img src="' . $url . '"';
		    foreach ( $atts as $key => $val )
		        $url .= ' ' . $key . '="' . $val . '"';
		    $url .= ' />';
		}
		return $url;
	}