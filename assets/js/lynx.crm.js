var callbacks = {};

$(function() {
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


    $('#contact_address_fill').bind('click', function() {
        var cid = $('#cont_company_id').val();

        if(cid == '') {
            alert('You need to have a company selected to do this!');
            return;
        }

        $.getJSON('/func/contact/address/'+cid, function(data){
            $('#cont_default_address_1').val(data.comp_default_address_1);
            $('#cont_default_address_2').val(data.comp_default_address_2);
            $('#cont_default_address_3').val(data.comp_default_address_3);
            $('#cont_default_address_city').val(data.comp_default_address_city);
            $('#cont_default_address_county').val(data.comp_default_address_county);
            $('#cont_default_address_postcode').val(data.comp_default_address_postcode);
            $('#cont_default_address_country').val(data.comp_default_address_country);
        });
    });

    $('#activate_postcode_lookup').bind('click', function(){
       $('.postcode_lookup_container').toggle();
    });

    $('#postcode_lookup').getAddress({
        api_key: 'YiBMdQOvqkKw3zlyGcMj7g586',
        output_fields:{
            line_1: '#cont_alt_address_1',
            line_2: '#cont_alt_address_2',
            line_3: '#cont_alt_address_3',
            post_town: '#cont_alt_address_city',
            postcode: '#cont_alt_address_postcode',
            county: '#cont_alt_address_county'
        },
        onAddressSelected: function() {
            $('.postcode_lookup_container').toggle();
        }
    });

    // Instantiate Select Picker From bootstrap-select.min.js
    $('.selectpicker').selectpicker();

    //$(JSON.parse($('[name="post-load"]').html())).each(function(a,b){
    //   $('head').append('<script type="text/javascript" src="'+b+'"></script>');
    //});

    $('.bs-select').bootstrapSwitch({
        'size': 'small',
        'onColor': 'success'
    });

    _selectAppendInit('.selectpicker');

    var $th = $('.typeahead');
    $th.typeahead({source:['Bedfordshire', 'Berkshire', 'Buckinghamshire', 'Cambridgeshire', 'Cheshire', 'Cornwall', 'Cumberland', 'Derbyshire', 'Devon', 'Dorset', 'Durham', 'Essex', 'Gloucestershire', 'Hampshire', 'Herefordshire', 'Hertfordshire', 'Huntingdonshire', 'Isle of Ely', 'Isle of Wight', 'Kent', 'Lancashire', 'Leicestershire', 'Lincolnshire – Parts of Holland', 'Lincolnshire – Parts of Kesteven', 'Lincolnshire – Parts of Lindsey', 'London', 'Middlesex', 'Monmouthshire', 'Norfolk', 'Northamptonshire', 'Northumberland', 'Nottinghamshire', 'Oxfordshire', 'Rutland', 'Shropshire', 'Soke of Peterborough', 'Somerset', 'Staffordshire', 'East Suffolk', 'West Suffolk', 'Surrey', 'East Sussex', 'West Sussex', 'Warwickshire', 'Westmorland', 'Wiltshire', 'Worcestershire', 'Yorkshire – East Riding', 'Yorkshire – North Riding', 'Yorkshire – West Riding'],
        autoSelect: true});
    $th.change(function() {
        var current = $th.typeahead("getActive");
        if (current) {
            // Some item from your model is active!
            if (current.name == $input.val()) {
                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
            }
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
        }
    });

    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $('#crm_modal .modal-content').html($('#modal-template').html());
    $('#crm_modal').on('loaded.bs.modal', function() {
        $('form',this).attr('action', $('[data-modal-form]', this).attr('data-modal-form'));
        $('.selectpicker',this).selectpicker();
        $('.datetimepicker',this).datetimepicker({
            'format': 'DD/MM/YYYY H:mm A'
        });

        $('form',this).unbind().bind('submit', function(){
            var obj = $(this).serializeObject();
            obj.action = 'process';
            $.post($(this).attr('action'),obj,function(response){
                callbacks.modal(response);
            },'json');
           return false;
        });
        
        $('[data-model-reload]', this).unbind().bind('click', function(){
	        var h = $(this).attr('href');
	        $('#crm_modal .modal-content').html($('#modal-template').html());
	        $('#crm_modal .modal-content').load(h, function(){
		        $('#crm_modal').trigger('loaded.bs.modal');
	        });
	        return false;
        });
    });

    $('#crm_modal').on('hide.bs.modal', function() {
        $(this).removeData('bs.modal');
        $('#crm_modal .modal-content').html($('#modal-template').html());
    });
    
    $('[data-tab-set-active]').trigger('click');

    if($('#saveEntity').size()){
        $('#saveEntity').bind('click', function(){
            $.post(window.location.href,$('#'+entity+'Form').serializeObject(),function(response){
                callbacks[entity].submit(response);
            },'json');
        });
        $('input,select',$('#'+entity+'Form')).bind('change',function(){
            $('#saveEntity').removeClass('flash');
            $('#saveEntity').addClass('animated');
            $('#saveEntity').addClass('flash');
            $('#saveEntity').css('color', "red");
        });
    }

    callbacks.modal = function(response) {
        if (response.error) {
            alert(response.error);
            return;
        }

        if (!response.removed) {
          //alert('Saved');
        }

        if(response.entity_id){
            ajaxReloadNoteList(response.entity, response.entity_id);
            ajaxReloadTaskList(response.entity, response.entity_id);
            ajaxReloadCommList(response.entity, response.entity_id);
        }

        if(response.deleted){
            window.location.reload();
            return;
        }

        $('#crm_modal [data-dismiss="modal"]').trigger('click');
        //reload the tab for the section somehow .. lol
    };

    callbacks.company = {
      submit : function(response){
          if(response.error){
              alert(response.error);
              return;
          }
          if(response.redirect){
              window.location = response.redirect;
          }
          //alert('Saved');
          $('#saveEntity').removeAttr('style');
      }
    };

    callbacks.contact = {
        submit : function(response){
            if(response.error){
                alert(response.error);
                return;
            }
            if(response.redirect){
                window.location = response.redirect;
            }
            //alert('Saved');
            $('#saveEntity').removeAttr('style');
        }
    };


    var fs = $('#startFullScreen'),
        ns = $('#exitFullScreen');

    $(document).on('mozfullscreenchange webkitfullscreenchange fullscreenchange', function(e){

        chkfullscreen();

    });

});

// Find the right method, call on correct element
function launchFullScreen(element) {

    if (element.requestFullscreen){
        element.requestFullscreen();
    } else if (element.mozRequestFullScreen){ 
        element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}

var ajaxReloadNoteList = function(entity, entity_id){
    if(!$('[data-partial-note-list]').size()){
        return;
    }
    $.getJSON($('base').attr('url')+'ajax/note/list/'+entity+'/'+entity_id, function(e){
        $('[data-partial-note-list]').replaceWith(e.view);
    });
};

var ajaxReloadTaskList = function(entity, entity_id){
    if(!$('[data-partial-task-list]').size()){
        return;
    }
    $.getJSON($('base').attr('url')+'ajax/task/list/'+entity+'/'+entity_id, function(e){
        $('[data-partial-task-list]').replaceWith(e.view);
    });
};
var ajaxReloadCommList = function(entity, entity_id){
    if(!$('[data-partial-comm-list]').size()){
        return;
    }
    $.getJSON($('base').attr('url')+'ajax/comm/list/'+entity+'/'+entity_id, function(e){
        $('[data-partial-comm-list]').replaceWith(e.view);
    });
};